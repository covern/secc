.PHONY: all test clean mrproper parser check-dependencies

MILL = ./mill

SECC_EXAMPLES = examples

VPATH     	= $(SECC_EXAMPLES)
SECC_C    	= $(wildcard $(SECC_EXAMPLES)/*.c)
SECC_OBJ  	= $(SECC_C:$(SECC_EXAMPLES)/%.c=obj/%.o)
SECC_JAVA 	= secc/src/secc/c/Parser.java \
              secc/src/secc/c/Scanner.java
SECC_SCALA	= $(shell find secc/src -iname "*.scala")

CC ?= cc
CFLAGS += -Iinclude -Wall -W -Wpedantic -g -fsanitize=address -fno-omit-frame-pointer -O2

BEAVER = ./beaver
JFLEX  = ./jflex

SECC_JAR = out/secc/jar.dest/out.jar

SECC_LAUNCHER = out/secc/launcher.dest/run
SECC_SH  = ./SecC.sh

all: $(SECC_JAR) $(SECC_SH)

parser: $(SECC_JAVA)

test:
	$(MILL) secc.test

clean:
	$(MILL) clean
	rm -f $(SECC_SH)

mrproper: clean
	rm -f $(SECC_JAVA)

check-dependencies:
	$(MILL) mill.scalalib.Dependency/updates

$(SECC_LAUNCHER):
	@echo $@
	$(MILL) secc.launcher

$(SECC_JAR): $(SECC_JAVA) $(SECC_SCALA)
	@echo $@
	$(MILL) secc.jar

$(SECC_SH): $(SECC_LAUNCHER)
	@echo "[echo]  $@"
	@echo "#!/usr/bin/env bash" > $@
	@echo "export LD_LIBRARY_PATH=$(PWD)/secc/lib" >> $@
	@echo "export PATH=.:\$$PATH" >> $@
	@echo "source $(PWD)/$(SECC_LAUNCHER)" >> $@
	@echo "[chmod] $@"; chmod +x $@

%.java: %.grammar
	$(BEAVER) -t $^

%.java: %.flex
	$(JFLEX) -nobak $^

o: $(SECC_OBJ)
	@echo $(SECC_OBJ)

# Note compilation produces (only) unused variable warnings
auction.o: examples/case-studies/auction.c
	$(CC) $(CFLAGS) $^ -c -o $@

auction-main.o: examples/auction-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

auction-main: auction-main.o auction.o
	$(CC) $(CFLAGS) $(LDFLAGS) -pthread $^ -o $@

private-learning.o: examples/case-studies/private-learning.c
	$(CC) $(CFLAGS) $^ -c -o $@

learning-main.o: examples/learning-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

learning-main: learning-main.o private-learning.o
	$(CC) $(CFLAGS) $(LDFLAGS) -pthread $^ -o $@

rb.o: examples/case-studies/rb.c
	$(CC) $(CFLAGS) $^ -c -o $@

cddc-main.o: examples/cddc-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

cddc-main: cddc-main.o rb.o
	$(CC) $(CFLAGS) $(LDFLAGS) -pthread $^ -o $@

ct.o: examples/case-studies/ct.c
	$(CC) $(CFLAGS) $^ -c -o $@

memcmp-main.o: examples/memcmp-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

memcmp-main: memcmp-main.o ct.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

location.o: examples/case-studies/location-server.c
	$(CC) $(CFLAGS) $^ -c -o $@

location-main.o: examples/location-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

location-main: location-main.o location.o
	$(CC) $(CFLAGS) $(LDFLAGS) -pthread $^ -o $@	

clasloc.o: examples/case-studies/clasloc.c
	$(CC) $(CFLAGS) $^ -c -o $@

clasloc-main.o: examples/clasloc-main.c
	$(CC) $(CFLAGS) $^ -c -o $@

clasloc-main:  clasloc-main.o clasloc.o
	$(CC) $(CFLAGS) $(LDFLAGS) -pthread $^ -o $@

wordle.o: examples/case-studies/wordle.c
	$(CC) $(CFLAGS) $^ -c -o $@

wordle-main:  wordle-main.o wordle.o
	$(CC) $(CFLAGS) $(LDFLAGS) -lreadline -pthread $^ -o $@
