package secc.c

import java.io.File

package object test {
  val dirs = List(
    "examples/regression",
    "examples/data-structures",
    "examples/case-studies"
  )

  val exclude = List(
    "casino-presentation.c"
  )

  val tests =
    for (
      dir <- dirs;
      file <- new File(dir).list()
      if (file endsWith ".c") && !(exclude contains file)
    )
      yield dir + "/" + file
}
