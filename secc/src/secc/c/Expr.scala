package secc.c

sealed trait Expr extends Assert {
}

case class Field(typ: Type, name: String, ghost: Boolean) extends beaver.Symbol {
  override def toString = typ + " " + name
}

case class Formal(typ: Type, name: String) extends beaver.Symbol {
  def pair = (Id(name), typ)
  override def toString = typ + " " + name
}

case class Lit(arg: Any) extends Expr {
  override def toString = arg.toString
}

case class Id(name: String) extends Expr {
  override def toString = name
}

object Id {
  val wild = Id("_")
  val result = Id("result")
  val main = Id("main")
}

case class PreOp(op: String, arg: Expr) extends Expr {
  override def toString = "(" + op + " " + arg + ")"
}

case class PostOp(op: String, arg: Expr) extends Expr {
  override def toString = "(" + arg + " " + op + ")"
}

case class BinOp(op: String, arg1: Expr, arg2: Expr) extends Expr {
  override def toString = "(" + arg1 + " " + op + " " + arg2 + ")"
}

case class Index(base: Expr, index: Expr) extends Expr {
  override def toString = base + "[" + index + "]"
}

case class Update(base: Expr, index: Expr, arg: Expr) extends Expr {
  override def toString = base + "[" + index + ":=" + arg + "]"
}

case class Question(test: Expr, left: Expr, right: Expr) extends Expr {
  override def toString = "(" + test + " ? " + left + " : " + right + ")"
}

case class SizeOfType(typ: Type) extends Expr {
  def free = Set()
  override def toString = "sizeof(" + typ + ")"
}

case class SizeOfExpr(expr: Expr) extends Expr {
  override def toString = "sizeof(" + expr + ")"
}

case class Cast(typ: Type, expr: Expr) extends Expr {
  override def toString = "(" + typ + ")" + expr
}

case class Dot(expr: Expr, field: String) extends Expr {
  override def toString = expr + "." + field
}

case class Arrow(expr: Expr, field: String) extends Expr {
  override def toString = expr + "->" + field
}

case class FunCall(fun: Id, args: List[Expr]) extends Expr { // no function pointers
  def this(name: String, args: Array[Expr]) = this(Id(name), args.toList)
  override def toString = fun + args.mkString("(", ", ", ")")
}

case class Init(values: List[(Option[String], Expr)]) extends Expr { // { .field = value } or { value }
}

case class Old(inner: Expr) extends Expr {
  override def toString = "old(" + inner + ")"
}

case class Bind(how: String, params: List[Formal], body: Expr) extends Expr {
  def this(how: String, params: Array[Formal], body: Expr) = this(how, params.toList, body)

  def bound = Set(params map (p => Id(p.name)): _*)

  override def toString = {
    how + params.mkString(" ", ", ", ". ") + body
  }
}

case class Placeholder(bound: Option[Id]) extends Expr {
  def this() = this(None)
  def this(id_ : Id) = this(Some(id_))
  
  override def toString = bound match {
    case None => "?"
    case Some(id) => "?" + id
  }
}

object Post {
  // def apply(traces: List[Trace], ids: Set[Id]): List[Trace] = {
  //   for(Trace(test, regex) <- traces)
  //     yield Trace(Post(test, ids), regex)
  // }

  def apply(expr: Expr, ids: Set[Id]): Expr = expr match {
    case id: Id => if (ids contains id) id else Old(id)
    case _: Lit => expr
    case Old(inner) => ???
    case Placeholder(_) => expr
    case PreOp(op, arg) => PreOp(op, apply(arg, ids))
    case PostOp(op, arg) => PostOp(op, apply(arg, ids))
    case BinOp(op, arg1, arg2) => BinOp(op, apply(arg1, ids), apply(arg2, ids))
    case Index(base, index) => Index(apply(base, ids), apply(index, ids))
    case Update(base, index, arg) => Update(apply(base, ids), apply(index, ids), apply(arg, ids))
    case Question(test, left, right) => Question(apply(test, ids), apply(left, ids), apply(right, ids))
    case Cast(typ, expr) => Cast(typ, apply(expr, ids))
    case SizeOfExpr(expr) => expr
    case SizeOfType(typ) => expr
    case Arrow(expr, field) => Arrow(apply(expr, ids), field)
    case Dot(expr, field) => Dot(apply(expr, ids), field)
    case FunCall(name, args) => FunCall(name, args map (apply(_, ids)))
    case Init(values) => Init(values map { case (name, expr) => (name, apply(expr, ids)) })
    case bind @ Bind(how, params, body) => Bind(how, params, apply(body, ids ++ bind.bound))
  }

  def apply(assrt: Assert, ids: Set[Id]): Assert = assrt match {
    case expr: Expr => apply(expr, ids)
    case And(left, right) => And(apply(left, ids), apply(right, ids))
    case Cond(test, left, right) => Cond(apply(test, ids: Set[Id]), apply(left, ids), apply(right, ids))
    case PointsTo(ptr, sec, arg) => PointsTo(apply(ptr, ids), apply(sec, ids), apply(arg, ids))
    case PointsToRange(ptr, len, sec, arg) => PointsToRange(apply(ptr, ids), apply(len, ids), apply(sec, ids), apply(arg, ids))
    case Chunk(pred, in, out) => Chunk(pred, in map (apply(_, ids)), out map (apply(_, ids)))
    case bind @ Exists(params, body) => Exists(params, apply(body, ids ++ bind.bound))
  }
}
