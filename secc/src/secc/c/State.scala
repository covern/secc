package secc.c


import scala.language.postfixOps

import secc.error
import secc.re
import secc.heap.Heap
import secc.heap.Pred
import secc.heap.Prop
import secc.pure.Pure
import secc.pure.Solver
import secc.pure.Sort
import secc.pure.Relational

case class Box(assrt: Assert, rely: Expr, guarantee: Expr, inst: Store, params: Store) {
  def update(newInst: Store) = copy(inst = newInst filter { case (k,v) => inst contains k })
  override def toString = assrt.toString + " | " + inst.mkString(", ")
}

object Box {
  val empty = Box(True, True, True, Map(), Map())
}

case class State(
  attacker: Pure,
  path: List[Pure],
  store: Store,
  old: List[Store],
  heap: Heap,
  box: Box,
  numAtomicCalls: Int,
  ct: Boolean,
  trace: re.Expr,
  solver: Relational) {

  override def toString = {
    attacker + " | " + store.mkString(", ") + " | " + (path ++ heap.props).mkString(" && ")
  }

  def info() {
    log.info("state")
    log.shift {
      
      log.info("store")
      log.shift {
        for((x, e) <- store)
          log.info(x + " -> " + e)
      }

      log.info("path")
      log.shift {
        for(pure <- path)
          log.info(pure)
      }

      log.info("heap  ")
      log.shift {
        for(prop <- heap.props)
          log.info(prop)
      }

      log.info("events ", trace)
      log.info("shared ", box.toString)
    }
  }

  def saveOld = {
    copy(old = store :: old)
  }

  def high = {
    copy(attacker = secc.pure.High)
  }

  def isPure = {
    heap == Heap.empty
  }

  def pure = {
    copy(heap = Heap.empty)
  }
  
  def updateBox(inst: Store) = {
    copy(box = box update inst)
  }

  def havocBox(ctx: Context): State = {
    val newInst = box.inst map (t => (t._1, ctx arbitrary t._1))
    updateBox(newInst) assign newInst
  }

  def getSolver(relational: Boolean) = {
    if (relational) solver else solver.inner
  }

  def withSolver[A](relational: Boolean)(f: Solver => A): A = {
    // XXX: Note that the non-relational solver cannot deal with Pure.haslabel (and polymorphic types in particular)
    var which = getSolver(relational)

    which scoped {
      which match {
        case which: Relational =>
          which assumeAttacker attacker
        case _ =>

      }

      which assume path

      val ptrs = heap.props collect {
        case secc.heap.PointsTo(ptr, _, _) => ptr
      }

      if (!ptrs.isEmpty) {
        // println(ptrs)
        ptrs foreach {
          _.typ match {
            case Sort.pointer(elem) =>
            case _ => ???
          }
        }
        which assumeDistinct ptrs
      }

      f(which)
    }
  }

  def maybeConsistent: List[State] = {
    withSolver(relational = false) {
      solver =>
        if (solver.isConsistent) List(this)
        else Nil
    }
  }

  // TODO: this gets called way too often?
  def stronglyConsistent: List[State] = {
    withSolver(relational = true) {
      solver =>
        if (solver.isConsistent) List(this)
        else Nil
    }
  }

  def check(phi: Pure): Boolean = {
    withSolver(phi.isRelational) {
      solver =>
        solver isValid phi
    }
  }

  // def _add(todo: List[Pure], done: List[Pure]): List[State] = todo match {
  //   case Nil =>
  //     if(done.isEmpty)
  //       List(this)
  //     else
  //       copy(path = done ++ path) maybeConsistent
  //   case False :: _ =>
  //     Nil
  //   case True :: rest =>
  //     _add(rest, done)
  //   case Pure.and(left, right) :: rest =>
  //     _add(left :: right :: rest, done)
  //   case phi :: rest =>
  //     _add(rest, phi :: done)
  // }

  def &&(that: Pure): List[State] = that match {
    // _add(List(that), Nil)
    case secc.pure.True =>
      List(this)
    case secc.pure.False =>
      Nil
    case Pure.haslabel(arg, secc.pure.High) =>
      List(this)
    case Pure.and(left, right) =>
      for(st1 <- this && left; st2 <- st1 && right)
        yield st2
    case _ =>
      copy(path = that :: path) maybeConsistent
  }

  def &&(that: Prop) = {
    copy(heap = heap && that) maybeConsistent
  }

  def &&(that: Heap) = {
    copy(heap = heap && that) maybeConsistent
  }

  def assign(id: Id, arg: Pure) = {
    copy(store = store + (id -> arg))
  }

  def assign(asg: Iterable[(Id, Pure)]) = {
    copy(store = store ++ asg)
  }

  def havoc(id: Id, ctx: Context) = {
    val pair = (id, ctx arbitrary id)
    copy(store = store + pair)
  }

  def havoc(ids: Iterable[Id], ctx: Context, checked: Boolean = true) = {
    val pairs = ids map (id => (id, ctx arbitrary (id, checked)))
    copy(store = store ++ pairs)
  }

  def load(ptr: Pure): (Pure, Pure) = {
    heap load (ptr, check) match {
      case None =>
        throw error.VerificationFailure("memory", "illegal dereference", ptr, this)
      case Some((sec, arg)) =>
        (sec, arg)
    }
  }

  def store(ptr: Pure, arg: Pure): (Pure, Pure, State) = {
    val (res, rest) = heap store (ptr, arg, check)
    res match {
      case None =>
        throw error.VerificationFailure("memory", "illegal dereference", ptr, this)
      case Some((sec, old)) =>
        (sec, old, copy(heap = rest))
    }
  }

  def access(ptr: Pure) = {
    val (pto, rest) = heap access (ptr, check)
    pto match {
      case None =>
        throw error.VerificationFailure("memory", "cannot find ", ptr + " |-> ?", this)
      case Some(pto) =>
        (pto, copy(heap = rest))
    }
  }

  def access(ptr: Pure, len: Pure) = {
    val (pto, rest) = heap access (ptr, len, check)
    pto match {
      case None =>
        throw error.VerificationFailure("memory", "cannot find ", ptr + " |=>[" + len + "] ?", this)
      case Some(pto) =>
        (pto, copy(heap = rest))
    }
  }

  def access(pred: Pred, in: List[Pure]) = {
    val (chunk, rest) = heap access (pred, in, check)
    chunk match {
      case None =>
        throw error.VerificationFailure("memory", "cannot find ", pred + "(" + in.mkString(",") + "; ?)", this)
      case Some(chunk) =>
        (chunk, copy(heap = rest))
    }
  }

  /* 

  def emit(event: String) = {
    val sym = re.Sym(event)
    val trace_ = trace derive sym
    if(trace_ == re.Emp)
        throw error.VerificationFailure("events", "unmatched event ", event, trace, this)
    copy(trace = trace_)
  } */

  def clearTrace = {
    copy(trace = re.Eps)
  }

  def fromTrace(expr: re.Expr) = {
    assert(trace == re.Eps)
    copy(trace = expr)
  }

  def emit(expr: re.Expr): State = {
    copy(trace = trace ~ expr)
  }

  def emit(event: String): State = {
    val sym = re.Sym(event)
    emit(sym)
  }

  def acceptedBy(expr: re.Expr) = {
    // println("checking subset")
    // println("  trace " + trace)
    // println("  spec  " + expr)
    trace subsetOf expr match {
      case None =>
        this
      case Some((cex, None, rest, pat)) => 
        throw error.VerificationFailure("events", "unmatched trace",  cex.mkString(" ") + " . " + rest, "residual specification", pat, this)
      case Some((cex, Some(sym), rest, pat)) => 
        throw error.VerificationFailure("events", "unmatched trace", cex.mkString(" ") + " . " + sym, "residual produced", rest, "residual specification", pat, this)
    }
  }
  
  // def skipPrefix(expr: re.Expr) = {
  //   trace derive expr match {
  //     case Left(trace_) =>
  //       copy(trace = trace_)
  //     case Right((cex, None, rest, pat)) => 
  //       throw error.VerificationFailure("events", "unmatched trace",  cex.mkString(" ") + " . " + rest, "residual specification", pat, this)
  //     case Right((cex, Some(sym), rest, pat)) => 
  //       throw error.VerificationFailure("events", "unmatched trace", cex.mkString(" ") + " . " + sym, "residual produced", rest, "residual specification", pat, this)
  //   }
  // }

  def resetAtomicCallCounter(): State = {
    copy (numAtomicCalls = 0)
  }

  def doAtomicCall(): State = {
    if (numAtomicCalls != 0) {
      throw error.VerificationFailure("effects", "multiple atomic calls in same atomic block")
    }
    copy (numAtomicCalls = numAtomicCalls + 1)
  }
}

object State {
  def empty(ctime: Boolean) = State(
    attacker = secc.pure.Attacker,
    path = Nil,
    store = Map.empty,
    old = Nil,
    heap = Heap.empty,
    box = Box.empty,
    numAtomicCalls = 0,
    ct = ctime,
    trace = re.Eps,
    solver = Solver.relational)

  def default(ct: Boolean) = empty(ct)
}