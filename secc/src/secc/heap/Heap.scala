package secc.heap

import secc.error
import secc.pure.Pure
import secc.pure.Ren
import secc.pure.Subst

case class Heap(props: List[Prop]) {
  def free = Set((props flatMap (_.free)): _*)
  def rename(re: Ren) = Heap(props map (_ rename re))
  def subst(su: Subst) = Heap(props map (_ subst su))
  
  // def ptrs = pto map (_.ptr)

  def &&(prop: Prop): Heap = Heap(prop :: props)
  def &&(that: Heap): Heap = Heap(this.props ++ that.props)

  /** Prepend props to this heap, but keep the order of props (no reversing) */
  def &&(props: List[Prop]): Heap = Heap(props ++ this.props)

  def load(where: Pure, test: Pure => Boolean): Option[(Pure, Pure)] = {
    props collectFirst {
      case PointsTo(ptr, sec, arg) if ptr.typ == where.typ && test(ptr === where) =>
        (sec, arg)
      case PointsToRange(ptr, len, sec, arg) if ptr.typ == where.typ && test((ptr beforeeq where) && (where before (ptr index len))) =>
      val idx = where offsetof ptr
        (sec, arg select idx)
    }
  }

  def store(where: Pure, arg: Pure, test: Pure => Boolean): (Option[(Pure, Pure)], Heap) = {
    val (res, upd) = Heap.store(where, arg, props, test)
    (res, Heap(upd))
  }

  def access(where: Pure, test: Pure => Boolean): (Option[PointsTo], Heap) = {
    val (res, rest) = Heap.access(where, props, test)
    (res, Heap(rest))
  }

  def access(where: Pure, len: Pure, test: Pure => Boolean): (Option[PointsToRange], Heap) = {
    val (res, rest) = Heap.access(where, len, props, test)
    (res, Heap(rest))
  }

  def access(what: Pred, in: List[Pure], test: Pure => Boolean): (Option[Chunk], Heap) = {
    val (res, rest) = Heap.access(what, in, props, test)
    (res, Heap(rest))
  }
}

object Heap {
  val empty = Heap(Nil)

  def store(where: Pure, arg: Pure, heap: List[Prop], test: Pure => Boolean): (Option[(Pure, Pure)], List[Prop]) = heap match {
    case Nil =>
      (None, heap)
    case (pto@PointsTo(ptr, sec, rhs)) :: heap if ptr.typ == where.typ && test(ptr === where) =>
      (Some((sec, rhs)), PointsTo(ptr, sec, arg) :: heap)
    case PointsToRange(ptr, len, sec, rhs) :: heap if ptr.typ == where.typ && test((ptr beforeeq where) && (where before (ptr index len))) =>
      val idx = where offsetof ptr
      (Some((sec, rhs select idx)), PointsToRange(ptr, len, sec, rhs store (idx, arg)) :: heap)
    case pto :: heap =>
      val (res, rest) = store(where, arg, heap, test)
      (res, pto :: rest)
  }

  /**
   * Find and remove a points to assertion from the given heap,
   * such that its location is equal to loc according to the provided test predicate.
   *
   * If such an assertion exists, the result is that assertion and the remaining heap without it,
   * otherwise, the original heap is returned.
   */
  def access(where: Pure, heap: List[Prop], test: Pure => Boolean): (Option[PointsTo], List[Prop]) = heap match {
    case Nil =>
      (None, heap)
    case (pto@PointsTo(ptr, _, _)) :: heap if ptr.typ == where.typ && test(ptr === where) =>
      // XXX: these two types might mismatch when fields are in play
      (Some(pto), heap)
    case pto :: heap =>
      val (res, rest) = access(where, heap, test)
      (res, pto :: rest)
  }

    def access(where: Pure, len: Pure, heap: List[Prop], test: Pure => Boolean): (Option[PointsToRange], List[Prop]) = heap match {
    case Nil =>
      (None, heap)
    case (pto@PointsToRange(ptr, len_, _, _)) :: heap if ptr.typ == where.typ && test(ptr === where && len === len_) =>
      // XXX: these two types might mismatch when fields are in play
      (Some(pto), heap)
    case pto :: heap =>
      val (res, rest) = access(where, len, heap, test)
      (res, pto :: rest)
  }

  def access(what: Pred, in: List[Pure], heap: List[Prop], test: Pure => Boolean): (Option[Chunk], List[Prop]) = heap match {
    case Nil =>
      (None, heap)
    case (chunk@Chunk(pred, args, _)) :: heap if pred == what && test(Pure.eqs(args, in)) =>
      (Some(chunk), heap)
    case chunk :: heap =>
      val (res, rest) = access(what, in, heap, test)
      (res, chunk :: rest)
  }
}