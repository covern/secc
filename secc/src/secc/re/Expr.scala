package secc.re

sealed trait Expr extends beaver.Symbol {
  def syms: Set[Sym]
  def first: Set[Sym]
  def nullable: Boolean
  def derive(sym: Sym): Expr

  def *() = this match {
    case Emp | Eps => Eps
    case Rep(_)    => this
    case _         => Rep(this)
  }

  def |(that: Expr) = Or(Set(this, that))

  def ~(that: Expr) = (this, that) match {
    case (Emp, _)                    => Emp
    case (_, Emp)                    => Emp
    case (Eps, _)                    => that
    case (_, Eps)                    => this
    // case (Seq(first, second), third) => Seq(first, Seq(second, third))
    case _                           => Seq(this, that)
  }

  def derive(that: Expr) = Expr.suffix(that, this)
  def subsetOf(that: Expr) = Expr.simulate(this, that)
}

object Expr {
  // Simulates pat via transitions of expr,
  // where R collects all pairs of equivalent expressions seen along the way.
  // Terminates as soon as no new states are discovered,
  // in which case R is a forward simulation relation.

  // The check fails if pat becomes empty when expr is not,
  // returning an error trace as counterexample

  import scala.collection.mutable

  case class SimulationFailure(
      trace: List[Sym],
      sym: Option[Sym],
      rest: Expr,
      pat: Expr
  ) extends Throwable

  def simulate(
      expr: Expr,
      pat: Expr
  ): Option[(List[Sym], Option[Sym], Expr, Expr)] = {
    try {
      if (expr == Eps && !pat.nullable) {
        Some((Nil, None, expr, pat))
      } else {
        simulate(expr, pat, Nil, mutable.Set())
        None
      }
    } catch {
      case SimulationFailure(trace, sym, rest, pat) =>
        Some((trace.reverse, sym, rest, pat))
    }
  }

  def suffix(
      expr: Expr,
      pat: Expr
  ): Either[Expr, (List[Sym], Option[Sym], Expr, Expr)] = {
    try {
      if (expr == Emp) {
        Right(Nil, None, expr, pat)
      } else {
        Left(suffix(expr, pat, Nil, mutable.Set()))
      }
    } catch {
      case SimulationFailure(trace, sym, rest, pat) =>
        Right((trace.reverse, sym, rest, pat))
    }
  }

  // strips an expression from pat, yielding the suffix-language,
  // but fail as soon as the suffix language becomes empty
  def suffix(
      expr: Expr,
      pat: Expr,
      trace: List[Sym],
      R: mutable.Set[(Expr, Expr)]
  ): Expr = {
    assert(expr != Emp)

    if (R contains (expr, pat)) {
      // taken a full roundtrip, can ignore this!
      Emp
    } else {
      R += ((expr, pat))

      val syms = expr.first

      val res_ = for (sym <- syms) yield {
        val expr_ = expr derive sym
        val pat_ = pat derive sym

        if (pat_ == Emp)
          throw SimulationFailure(trace, Some(sym), expr, pat)

        suffix(expr_, pat_, sym :: trace, R)
      }

      if (expr.nullable) // includes expr == Eps
        Or(res_ + pat)
      else
        Or(res_)
    }
  }

  def simulate(
      expr: Expr,
      pat: Expr,
      trace: List[Sym],
      R: mutable.Set[(Expr, Expr)]
  ) {
    if (expr == pat) {
      // this should be fairly common in loops
      // println("ok: " + expr + " and " + pat)
    } else if (R contains (expr, pat)) {
      // seen and found ok
      // println("ok: " + expr + " and " + pat)
    } else {
      R += ((expr, pat))

      if(expr.nullable && !pat.nullable)
        throw SimulationFailure(trace, None, Eps, pat)

      val syms = expr.first
      // if(syms.isEmpty)
      //   println("ok: " + expr + " and " + pat)
      for (sym <- syms) {
        val expr_ = expr derive sym
        val pat_ = pat derive sym
        if (expr_ != Emp && pat_ == Emp)
          throw SimulationFailure(trace, Some(sym), expr, pat)
        if (expr_ == Eps && !pat_.nullable)
          throw SimulationFailure(sym :: trace, None, expr_, pat_)
        simulate(expr_, pat_, sym :: trace, R)
      }
    }
  }
}

object Or {
  def flatten(exprs: Set[Expr]): Set[Expr] = {
    exprs flatMap flatten
  }

  def flatten(expr: Expr): Set[Expr] = expr match {
    case Emp       => Set()
    case Or(exprs) => flatten(exprs)
    case _         => Set(expr)
  }

  def apply(exprs: Set[Expr]): Expr = {
    val exprs_ = flatten(exprs)
    exprs_.size match {
      case 0 => Emp
      case 1 => exprs_.head
      case _ => new Or(exprs_)
    }
  }
}

case object Emp extends Expr {
  def syms = Set()
  def first = Set()
  def nullable = false
  def derive(sym: Sym) = Emp
  override def toString = "∅"
}

case object Eps extends Expr {
  def self = this
  def syms = Set()
  def first = Set()
  def nullable = true
  def derive(sym: Sym) = Emp
  override def toString = "𝜖"
}

case class Sym(name: String) extends Expr {
  def syms = Set(this)
  def first = Set(this)
  def nullable = false
  def derive(sym: Sym) = if (sym == this) Eps else Emp
  override def toString = name
}

case class Rep(expr: Expr) extends Expr {
  def syms = expr.syms
  def first = expr.first
  def nullable = true
  def derive(sym: Sym) = (expr derive sym) ~ this
  override def toString = expr + "*"
}

case class Seq(left: Expr, right: Expr) extends Expr {
  def syms = left.syms ++ right.syms
  def nullable = left.nullable && right.nullable
  override def toString = "(" + left + " ~ " + right + ")"

  def first = if (left.nullable)
    left.first ++ right.first
  else
    left.first

  def derive(sym: Sym) = if (left.nullable)
    ((left derive sym) ~ right) | (right derive sym)
  else
    ((left derive sym) ~ right)
}

case class Or(exprs: Set[Expr]) extends Expr {
  def this(left: Expr, right: Expr) = this(Set(left, right))
  def syms = exprs flatMap (_.syms)
  def first = exprs flatMap (_.syms)
  def nullable = exprs exists (_.nullable)
  def derive(sym: Sym) = Or(exprs map (_ derive sym))
  override def toString = exprs mkString ("(", " | ", ")")
}
