int nondet();
  _(ensures result :: low)

void even_odd()
  _(trace (even odd) *)
{
  int b = 0;

  while(nondet() || b)
    _(invariant b :: low)
    _(trace (even odd) * if !b)
    _(trace (even odd) * even if  b)
  {
    if(!b) {
      _(emit even)
    } else {
      _(emit odd) 
    }

    b = !b;
  }
}