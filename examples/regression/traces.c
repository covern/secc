void another_trace()
  _(trace e f)
{
  _(emit e)
  _(emit f)
}

void missing_events1()
  _(trace a)
  _(fails events) // cannot call the failure "trace" because this is a reserved keyword
{}

void optional_events1()
  _(trace a *)
{}

void missing_events2()
  _(trace a b)
  _(fails events)
{
  _(emit a)
}

void invalid_events1()
  _(trace a b)
  _(fails events)
{
  _(emit a)
  _(emit c)
}

void ok_events1()
  _(trace a (b | c)*)
  // _(fails events)
{
  _(emit a)
  _(emit c)
  _(emit b)
  _(emit c)
}

void invalid_events2()
  _(trace a (b | c)*)
  _(fails events)
{
  _(emit a)
  _(emit c)
  _(emit b)
  _(emit x)
}

void trace_recursive1()
  _(trace a*)
{
  _(emit a)
  trace_recursive1();
}

void trace_1()
  _(trace (a* b c* d)*)
{
  int x;
  _(assume x :: low)
  if(x) {
    _(emit a)
    _(emit b) // we need to emit some b on this path!
    _(emit c)
    _(emit d)
    trace_1();
  } else {
    _(emit b)
    trace_2();
  }
}

void trace_2()
  _(trace (c* d) (a* b c* d)*)
{
  int x;
  _(assume x :: low)
  if(x) {
    _(emit c)
    trace_2();
  } else {
    _(emit d)
    trace_1();
  }
}

void with_trace()
  _(trace a b* (e | f)* c)
{
  _(emit a)
  int x;
  _(assume x :: low)
  while(x)
    _(trace b *)
  {
    _(emit b)
  }
  another_trace();
  _(emit c)
}

int nondet();
  _(ensures result :: low)

void even_odd()
  _(trace (even odd) *)
{
  int b = 0;

  while(nondet() || b)
    _(invariant b :: low)
    _(trace (even odd) * if !b)
    _(trace (even odd) * even if  b)
  {
    if(!b) {
      _(emit even)
    } else {
      _(emit odd) 
    }

    b = !b;
  }
}


void even_odd_wrong_start()
  _(trace (even odd) *)
  _(fails events)
{
  // this must fail!
  // the loop invariant assumes that we have seen an even event already
  int b = 1;

  while(nondet() || b)
    _(invariant b :: low)
    _(trace (even odd) * if !b)
    _(trace (even odd) * even if  b)
  {
    if(!b) {
      _(emit even)
    } else {
      _(emit odd)
    }

    b = !b;
  }
}

// check that return correctly verifies function traces
void loop_ret()
  _(trace a)
{
  while(1) {
    int b;
    _(assume b :: low)
    if(b) {
      _(emit a)
      return;
    }
  }
}

// check that return correctly verifies function traces
void loop_ret_wrong()
  _(trace a)
  _(fails events)
{
  while(1) {
    int b;
    _(assume b :: low)
    if(b) return;
  }
}

int event_callee();
    _(ensures result :: low)
    _(trace yes if  result)
    _(trace no  if !result)

int event_caller()
    _(trace yes yes | no no)
{
    // ensure that result is scoped correctly in the postcondition
    int b = event_callee();
    if(b) {
      _(emit yes)
    } else {
      _(emit no)
    }
}