void test(int *x, int i, int n)
  _(requires 0 <= i && i < n)
  _(requires exists map<int, int> _x.
    x |=>[n] _x)
  _(ensures exists map<int, int> _x.
    x |=>[n] _x)
{
  x[i] = 1;
}

void bzero(int *x, int n)
  _(requires 0 <= n && n :: low)
  _(requires exists map<int, int> _x.
    x |=>[n] _x)
  _(ensures exists map<int, int> _x.
    x |=>[n] _x)
  _(ensures forall int i.
    0 <= i && i < n ==> _x[i] == 0)
{
  int k = 0;

  while(k < n)
    _(invariant 0 <= k && k <= n && k :: low && n :: low)
    _(invariant exists map<int, int> _x.
      x |=>[n] _x)
    _(invariant forall int i.
      0 <= i && i < k ==> _x[i] == 0)
  {
    x[k++] = 0;
  }
}