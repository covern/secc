#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#define PORT 3000                    // TCP port to listen on
#define BUFSIZE 1024                 // size of buffer for reading from socket

pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

void acquire_lock(){
  pthread_mutex_lock(&g_mutex);
}

void release_lock(){
  pthread_mutex_unlock(&g_mutex);
}

enum answer {
  GREY,
  YELLOW,
  GREEN
};
typedef enum answer ans;

#define OUTPUT_SIZE (sizeof(ans)*5)

extern void run_game_for_player(int player, const char *word, ans *out);

struct thread_data{
  char *word;
  int player;
};

void* handle_incoming_connections(void *arg){
  struct thread_data *data = (struct thread_data *)arg;
  // the player id is actually the socket fd for the connection
  int connfd = data->player;
  ans *output = malloc(OUTPUT_SIZE);
  assert(output);
  run_game_for_player(connfd,data->word,output);
  // free the struct that was allocated in main
  free(data);
  return NULL;
}



void wait_for_guess(int player){
  const int connfd = player;
  // the player is the socket fd
  char guess[5];
  // get the data so we can see how much there is, but peek only so it
  // can subsequently be read in get_guess()
  int ret = recv(connfd, guess, sizeof(guess), MSG_PEEK);
  if (ret != 5){
    printf("wait_for_guess: didn't get expected number of bytes. pausing forever.");
    // wait forever since this player isn't guessing correctly
    pause();
  }
}
  

char *get_guess(int player){
  const int connfd = player;
  // the player is the socket fd
  char* guess = malloc(5); // must be malloc'd so it an outlive this function
  assert(guess);
  int ret = recv(connfd, guess, 5, 0);
  assert(ret == 5);
  return guess;
}

// responsible for freeing guess and answer, and closing the socket
void return_answer(int player, char *guess, ans *answer){
  // FIXME: move this conversion into the verified code
  char ascii[6];
  for (int i=0; i<5; i++){
    ascii[i] = (char)(answer[i])+'0';
  }
  ascii[5] = '\n'; // trailing newline as a courtesy
  // the player is the socket fd
  const int connfd = player;
  // send the response,  but only if doing so won't block
  if (fcntl(connfd, F_SETFL, O_NONBLOCK) != -1){
    send(connfd, ascii, sizeof(ascii), 0);
  }
  free(guess);
  free(answer);
  close(connfd);
}



int main(const int argc, const char *argv[]){
  if (argc != 2){
    printf("Usage: %s word\n",argv[0]);
    exit(1);
  }
  const char * const word = argv[1];
  if (strlen(word) != 5){
    printf("Word must be 5 characters long. Refusing to play.\n");
    exit(1);
  }

  // server setup for 
  // accepting connections
  socklen_t clilen;
  struct sockaddr_in servaddr, cliaddr; 
  pthread_t write_thread;
  int listenfd, connfd;  

  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(PORT);
  
  if (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0){
    fprintf(stderr,"Error binding socket to port %d. Exiting.\n", PORT);
    perror(NULL); exit(1);
  }
  if (listen(listenfd, 8) != 0){
    fprintf(stderr,"Error listening for connections on port %d. Exiting.\n", PORT);
    perror(NULL); exit(1);
  }

  printf("Server is running and waiting for connections on port %d\n", PORT);
    
  while(1){  
    clilen = sizeof(cliaddr);
    connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);
    struct thread_data * data = malloc(sizeof(struct thread_data));
    assert(data);
    data->player = connfd;
    data->word = strdup(word);
    assert(data->word);
    pthread_create(&write_thread, NULL, &handle_incoming_connections, (void *)data); 
  }
  
  return 0;
}
