_(function int length(list<int> xs))

_(rewrites
	length(nil) == 0;
	forall int x, list<int> xs. length(cons(x,xs)) == 1 + length(xs))

_(function int sum(list<int> xs))

_(rewrites
	sum(nil) == 0;
	forall int x, list<int> xs. sum(cons(x,xs)) == x + sum(xs))


/* tracks high-level view of the execution history */
_(predicate H(; list<int> xs))

/* specification of external functions */
int avg_get_input();
    _(requires exists list<int> tr. H(; tr))
    _(ensures  H(; cons(result, tr)))

void print_average(int value);
    _(requires value :: low)

struct avg_state { int count; int sum; };

/* the invariant protected by the lock */
_(predicate avg_inv(struct avg_state *st)
  exists int c, int s, int m, list<int> tr. H(; tr) &&
    &st->count |-> c && c :: low && c >= 0 &&
    &st->sum   |-> s &&
    c == length(tr) && s == sum(tr)
)

/* thread 1: read inputs and record them */
void avg_sum_thread() {
    while(1) {
      struct avg_state * st = avg_lock();
      _(unfold avg_inv(st))
      int i = avg_get_input();
      st->count += 1;
      st->sum   += i;
      _(fold avg_inv(st))  
      avg_unlock(st);
    }
}

/* minimum number of samples of which the average
   is considered secure to declassify */
int MIN_SAMPLES;

/* the declassification policy is formulated over
   the abstract history of the execution */
void safe_to_declassify(list<int> tr);
    _(requires length(tr) >= MIN_SAMPLES)
    _(ensures  sum(tr) / length(tr) :: low)
    _(lemma)

/* thread 2: declassify running average if it is secure */
void avg_declass_thread() {
    struct avg_state *st = avg_lock();
    _(unfold avg_inv(st))
    if (st->count >= MIN_SAMPLES) {
        _(assert exists list<int> tr. H(; tr))

        // appeal to the declassification policy here,
        // which requires us to prove its precondition length(tr) >= 6
        // _(apply safe_to_declassify(tr);)

        // we get an additional assumption: sum(tr) / length(tr) :: low
        // which is connected to the program's state by the invariant
        int avg = st->sum / st->count;
        _(assert avg :: low)

        print_average(avg);
    }
    _(fold avg_inv(st))  
    avg_unlock(st);
}






struct avg_state * avg_lock();
    _(ensures avg_inv(result))

void avg_unlock(struct avg_state *st);
    _(requires avg_inv(st))