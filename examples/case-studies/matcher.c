char next();
	_(ensures result :: low)

void check(char c);
	_(trace letter if 'a' <= c && c <= 'z')
	_(trace at     if c == '@')
	_(trace dot    if c == '.')
	_(trace eof    if c == 0)
	_(trace error  if (c < 'a' || 'z' < c) &&
	                   c != '@' && c != '.' && c != 0)

void abort();
	_(ensures false)

void lex()
	_(trace letter+ at letter+ dot letter+ eof)
{
	int state = 0;

	while(0 <= state && state <= 5)
		_(invariant state :: low)
		_(invariant 0 <= state && state <= 6)

		_(trace ()
			if state == 0)
		_(trace letter+
			if state == 1)
		_(trace letter+ at
			if state == 2)
		_(trace letter+ at letter+
			if state == 3)
		_(trace letter+ at letter+ dot
			if state == 4)
		_(trace letter+ at letter+ dot letter+
			if state == 5)
		_(trace letter+ at letter+ dot letter+ eof
			if state == 6)
	{
		char c = next();
		check(c);

		if(state == 0) {
			if('a' <= c && c <= 'z') state = 1;
			else                     abort();
		} else if(state == 1) {
			if('a' <= c && c <= 'z') state = 1;
			else if(c == '@')        state = 2;
			else                     abort();
		} else if(state == 2) {
			if('a' <= c && c <= 'z') state = 3;
			else                     abort();
		} else if(state == 3) {
			if('a' <= c && c <= 'z') state = 3;
			else if(c == '.')        state = 4;
			else                     abort();
		} else if(state == 4) {
			if('a' <= c && c <= 'z') state = 5;
			else                     abort();
		} else if(state == 5) {
			if('a' <= c && c <= 'z') state = 5;
			else if(c == 0)          state = 6;
			else                     abort();
		}
	}
}