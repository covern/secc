#include "secc.h"

/* the (so far) core of a trivial Wordle implementation

   we use SecC to specify exactly what information it should reveal to the
   player, when a guess is made, namely just the answer. We use SecC 
   to prove that only this information is leaked (plus that it is 
   functionally correct, i.e. it computes the correct answer).

   This is an illustrative example because functional correctness alone is 
   not sufficient: the straightforward implementation would leak too much
   information about yellow tiles in its timing behaviour (namely which
   position the match occurs in).

   It is also illustrative because the specification of what the player
   is allowed to learn on each guess is non-trivial and depends on the
   relationship between the guess and the word, i.e. it inherently makes
   use of value-dependent classification and could not be specified 
   otherwise.

   Finally, it is further illustrative because it uses multiple security
   levels. Each player has a security level and the policy allows only
   leaking the information to the player who is making the guess (and not
   to other players). For this reason, the entire computation has to be
   constant-time. This also means that the specification of what the player
   is allowed to learn is subtle and can only be soundly specified using
   the new "_ :: "_ semantics.

   Effort required so far (for Toby) to write and prove this: ~ 2.5 days
   (including the unverified warpper code in wordle-main.c)
*/

enum answer {
  GREY,
  YELLOW,
  GREEN
};
typedef enum answer ans;

/* toint (below) is a workaround for SecC's type system */
#if !__SECC
#define toint(x)  (x)
#endif

#if __SECC
_(function list<int> snoc(int x, list<int> l))

_(rewrites
  forall int x. snoc(x,nil) == cons(x,nil);
  forall int x, int hd, list<int> tl. snoc(x,cons(hd,tl)) == cons(hd,snoc(x,tl)))

_(function list<int> append(list<int> a, list<int> b))
_(axioms
  forall list<int> b. append(nil,b) == b;
  forall int hd, list<int> tl, list<int> b. append(cons(hd,tl),b) == cons(hd,append(tl,b)))

_(function char bang(list<char> ls, int i))

_(rewrites
  forall int hd, list<char> tl. bang(cons(hd,tl),0) == hd;
  forall int hd, list<char> tl, int i. i > 0 ==> bang(cons(hd,tl),i) == bang(tl,i-1)
)

_(function int length(list<char> ls))

_(rewrites
  length(nil) == 0;
  forall int hd, list<char> tl. length(cons(hd,tl)) == length(tl)+1)

/* whether word contains the character c at some position < n in which the
   guess was not correct */
_(function bool contains(list<char> word, list<char> guess, char c, int n))

_(axioms
  forall list<char> word, list<char> guess, char c, int n. n == 0 ==> (contains(word,guess,c,n) <=> false);
  forall char w, list<char> word, char g, list<char> guess, char c, int n.
  n > 0 ==>
  (contains(cons(w,word),cons(g,guess),c,n) ==
   ((w != g && w == c) || contains(word,guess,c,n-1))))

/* what the player at level lev who has submitted the guess is assumed to be 
   able to learn, i.e. what information the game is allowed to release to them

   Note: this specification is rather subtle. In particuar, we cannot write
   (bang(word,i) != bang(guess,i)) ==> (contains(...) :: lev)
   The reason is because interpreting that implication in SecC makes a 
   case distinction on the LHS and that can be done only if we can prove
   the LHS is low which, of course, it is not. It is only known to the player
   at lev: it is not known publicly (and that is the point of this subjective
   policy that makes no information public except of course the lenghts of
   the words involved). */
_(predicate release_policy(list<char> word, list<char> guess; sec lev)
  (forall int i. i :: low ==> ((i >= 0 && i < length(word)) ==>
                               (bang(word,i) == bang(guess,i)) :: lev) &&
   ((contains(word,guess,bang(guess,i),length(word)) :: ((bang(word,i) != bang(guess,i)) ? lev : high)))))

_(function bool correct_output(list<char> word, list<char> guess, list<ans> out, int n))

_(axioms forall list<char> word, list<char> guess, list<ans> out, int n.
  correct_output(word, guess, out, n) ==
  (length(out) == n &&
   (forall int i. ((i >= 0 && i < n) ==>
                   (bang(out,i) == ((bang(guess,i) == bang(word,i)) ? GREEN :
                                    (contains(word,guess,bang(guess,i),length(word)) ? YELLOW : GREY)))))))


_(function list<ans> replicate(ans c,int n))
_(rewrites
  forall ans c, int n. n == 0 ==> replicate(c,n) == nil;
  forall ans c, int n. n > 0 ==> replicate(c,n) == cons(c,replicate(c,n-1)))

_(function list<ans> drop(list<ans> lst, int n))
_(axioms
  forall list<ans> lst, int n. n == 0 ==> drop(lst,n) == lst;
  forall int n. drop(nil,n) == nil;
  forall ans hd, list<ans> tl, int n. n > 0 ==> drop(cons(hd,tl),n) == drop(tl,n-1))

_(function list<ans> take(list<ans> lst, int n))
_(axioms
  forall list<ans> lst. take(lst,0) == nil;
  forall ans hd, list<ans> tl, int n. n > 0 ==> take(cons(hd,tl),n) == cons(hd,take(tl,n-1)))

/* clagged from regression/length-low.c */
void length_nonnegative(list<int> xs)
_(ensures length(xs) >= 0)
_(pure) _(lemma)
{
  if(xs != nil) {
    _(assert exists int y, list<int> ys. xs == cons(y, ys))
      length_nonnegative(ys);
  }
}
void length_zero_is_nil(list<int> xs)
  _(requires length(xs) == 0)
  _(ensures xs == nil)
  _(pure) _(lemma)
{
  if (xs == nil){
    return;
  }else{
    _(assert exists int y, list<int> ys. xs == cons(y,ys))
    length_nonnegative(ys);
    _(assert length(cons(y,ys)) > 0)
  }
}
void length_low_eq_nil_low(list<int> xs)
  _(requires length(xs)::low)
  _(ensures (xs == nil)::low)
  _(lemma)
{
  _(apply
    if(length(xs) == 0){
      length_zero_is_nil(xs);
    }else{
      _(assert length(xs) != 0)
      _(assert length(nil) == 0)
      _(assert xs != nil)
    }
  )
}
/* end clag from regression/length-low.c */
/* clag from case-studies/rb.c */
void length_nonzero_cons(list<int> xs)
_(requires length(xs) != 0)
_(ensures exists int y, list<int> ys. xs == cons(y,ys))
_(lemma) _(pure)
{
  if (xs == nil){
    _(assert length(xs) == 0)
  }else{
    _(assert exists int y, list<int> ys. xs == cons(y,ys))
  }
}
/* end clag from case-studies/rb.c */

void bang_tl(list<ans> lst, ans hd, list<ans> tl, int j)
  _(requires lst == cons(hd,tl))
  _(ensures j >= 0 ==> j < length(tl) ==> bang(tl,j) == bang(lst,j+1))
  _(lemma) _(pure)
{
  if (j >= 0) {
    _(assert j+1 > 0)
  }
}

void lst_elems_low_tl_elems_low(list<ans> lst, ans hd, list<ans> tl, sec lev)
  _(requires lst == cons(hd,tl))
  _(requires forall int i. i :: low ==> ((i >= 0 && i < length(lst)) ==> bang(lst,i) :: lev))
  _(requires length(lst) :: low)
  _(ensures forall int j. j :: low ==> ((j >= 0 && j < length(tl)) ==> bang(tl,j) :: lev))
  _(lemma)
{
  _(assert length(tl) == length(lst) - 1)
  _(apply forall int j. bang_tl(lst,hd,tl,j))
}

void list_low(list<ans> lst, sec lev)
  _(requires lev :: low)
  _(requires length(lst) :: low)
  _(requires forall int i. i :: low ==> ((i >= 0 && i < length(lst)) ==> bang(lst,i) :: lev))
  _(ensures lst :: lev)
  _(lemma)
{
  if (length(lst) == 0){
    length_zero_is_nil(lst);
  }else{
    length_nonnegative(lst);
    _(assert bang(lst,0) :: lev)
    length_nonzero_cons(lst);
    _(assert exists list<ans> tl. lst == cons(bang(lst,0),tl))
    _(assert length(tl) == length(lst) - 1)
    lst_elems_low_tl_elems_low(lst,bang(lst,0),tl,lev);
    list_low(tl, lev);
  }
}

void central_lemma(list<char> word, list<char> guess, list<ans> out, sec lev)
_(requires lev :: low)
_(requires length(word) :: low)
_(requires length(word) == length(guess))
_(requires correct_output(word, guess, out, length(word)))
_(requires release_policy(word, guess; lev))
_(ensures out :: lev)
_(lemma)
{
  _(unfold release_policy(word, guess; lev))
  list_low(out, lev);  
}

_(predicate ar(char *word; list<char> _word, int n)
  (n == 0 ==> _word == nil) &&
  (n > 0 ==> exists char hd, list<char> tl. _word == cons(hd,tl) &&
   word |-> hd &&
   ar(word+1;tl,n-1)))

_(function int bool_to_int(bool b))
_(axioms
  bool_to_int(true) == 1;
  bool_to_int(false) == 0)


_(function int abs_choose(int c, int a, int b))

_(axioms
  forall int c, int a, int b. c == 0 ==> abs_choose(c, a, b) == b;
  forall int c, int a, int b. c != 0 ==> abs_choose(c, a, b) == a)


void abs_choose_def2(int c, int a, int b)
_(requires c == 0 || c == 1)
_(ensures abs_choose(c, a, b) == ((c * a) + ((1 - c) * b)))
_(lemma) _(pure)
{
  if (c == 0){
  }else{
    _(assert c == 1)
  }
}

/* the solver can prove this directly, which is why do_guess doesn't
   need to invoke this lemma later on */
void abs_choose_def3(int c, int a, int b)
_(ensures abs_choose(c, a, b) == ((c != 0) ? a : b))
_(lemma) _(pure)
{
}

#endif

int choose_ct(int c, int a, int b)
  _(requires c == 0 || c == 1)
  _(ensures result == abs_choose(c, a, b))
  _(ensures result == a || result == b)
{
  
  _(apply abs_choose_def2(c, a, b);)
  return ((c * a) + ((1 - c) * b));
}

#if __SECC

/* hack to work around SecC's type system, clagged from case-studies/ct.c */
_(function int abs_toint(bool b))
_(axioms
  abs_toint(true) == 1;
  abs_toint(false) == 0)
int toint(bool b);
_(ensures result == abs_toint(b))

#endif // __SECC

int ccontains(char *word, char *guess, int n, char c)
_(requires n :: low)
_(requires exists list<char> _word. length(_word) == n && ar(word;_word,n))
_(requires exists list<char> _guess. length(_guess) == n && ar(guess;_guess,n))
_(ensures result == bool_to_int(contains(_word,_guess,c,n)))
_(ensures ar(word;_word,n) && ar(guess;_guess,n))
{
  if (n > 0){
    _(unfold ar(word;_word,n))
    _(unfold ar(guess;_guess,n))

    // some nonsense so we can derive that the tail lengths are low
    _(assert exists int _whd, list<int> _wtl. _word == cons(_whd,_wtl))
    _(assert length(_word) == 1 + length(_wtl))
    _(assert exists int _ghd, list<int> _gtl. _guess == cons(_ghd,_gtl))
    _(assert length(_gtl) == n - 1)
      
    int b = ccontains(word+1,guess+1,n-1,c);
    /* technically we are allowed to branch here but it's easier to avoid it
       to save having to convince the prover we are allowed to */
    int d = choose_ct(toint(word[0] == guess[0]), 0, toint(c == word[0]));
    _(fold ar(guess;_guess,n))
    _(fold ar(word;_word,n))
    int c = choose_ct(d,1,b);
    return c;
  }else{
    _(apply length_nonnegative(_word);)
    _(assert n == 0)
    return 0;
  }
}

#if __SECC
void ar_extend(char *word, list<char> _word, int n, char w)
_(requires n :: low && n >= 0)
_(requires ar(word;_word,n))
_(requires word + n |-> w)
_(ensures ar(word;snoc(w,_word),n+1))
  _(lemma)
{
  if (n == 0){
    _(unfold ar(word;_word,n))
    _(assert _word == nil)
    _(fold ar(word+1;nil,0))
    _(fold ar(word;snoc(w,nil),1))
  }else{
    _(unfold ar(word;_word,n))
    _(assert exists char hd, list<char> tl. _word == cons(hd,tl))
    ar_extend(word+1,tl,n-1,w);
    _(fold ar(word;snoc(w,_word),n+1))
  }
}


void correct_output_nil(list<char> word, list<char> guess)
_(ensures correct_output(word,guess,nil,0))
_(pure) _(lemma)
{
  _(assert length(nil) == 0)
  _(assert forall int i. ((i >= 0 && i < 0) ==> false))
}

void ar_expose(int *a, list<int> lst, int i, int n)
  _(requires ar(a; lst, n))
  _(requires n :: low && i :: low && i >= 0 && i < n)
  _(ensures ar(a; take(lst,i), i))
  _(ensures (a+i) |-> bang(lst,i))
  _(ensures ar(a+i+1; drop(lst,i+1), n - i - 1))
  _(lemma)
{
  _(assert n > 0)
  _(unfold ar(a; lst, n))
  _(assert exists int hd, list<int> tl. lst == cons(hd,tl))
  if (i == 0){
    _(fold ar(a; take(lst,0), 0))
  }else{
    _(assert i > 0)
    ar_expose(a+1, tl, i-1, n-1);
    _(fold ar(a; take(lst,i), i))
  }
}

void append_take_drop(list<int> a, int i)
  _(requires i >= 0 && i <= length(a))
  _(ensures append(take(a,i),drop(a,i)) == a)
  _(lemma) _(pure)
{
  if (i == 0){    
  }else{
    _(assert length(a) > 0)
    length_nonzero_cons(a);
    _(assert exists int hd, list<int> tl. a == cons(hd,tl))
    _(assert length(a) == length(tl) + 1)
    append_take_drop(tl, i-1);
  }
}

void ar_append(int *a, list<int> a1, int i, list<int> a2, int j)
  _(requires i >= 0 && i :: low && j :: low && j >= 0)
  _(requires ar(a+i; a2, j))
  _(requires ar(a; a1, i))
  _(ensures ar(a; append(a1,a2), i+j))
  _(lemma)
{
  if (i == 0){
    _(unfold ar(a; a1, i))
  }else{
    _(assert i > 0)
    _(unfold ar(a; a1, i))
    _(assert exists int hd, list<int> tl. a1 == cons(hd,tl))
    ar_append(a+1,tl,i-1,a2,j);
    _(fold ar(a; cons(hd,append(tl,a2)), i + j))
  }
}
void ar_take_extend(int *a, list<int> lst, int i)
_(requires i :: low && i >= 0 && i < length(lst))
_(requires ar(a; take(lst,i), i))
_(requires (a+i) |-> bang(lst,i))
_(ensures ar(a; take(lst,i+1), i+1))
_(lemma)
{
  _(assert length(lst) > 0)
  length_nonzero_cons(lst);
  _(assert exists int hd, list<int> tl. lst == cons(hd,tl))
  _(assert length(lst) == length(tl) + 1)
  if (i == 0){
    _(unfold ar(a; take(lst,0), 0))
    _(fold ar(a+1; nil, 0))
    _(fold ar(a; cons(hd,nil), 1))
  }else{
    _(assert i > 0)
    _(unfold ar(a; take(lst,i), i))
    ar_take_extend(a+1, tl, i-1);
    _(fold ar(a; take(lst,i+1), i+1))
  }
}

void ar_cover(int *a, list<int> lst, int i, int n)
  _(requires n :: low && i :: low && i >= 0 && i < n && n <= length(lst))
  _(requires ar(a; take(lst,i), i))
  _(requires (a+i) |-> bang(lst,i))
  _(requires ar(a+i+1; drop(lst,i+1), n - i - 1))
  _(ensures ar(a; lst, n))
  _(lemma)
{
  ar_take_extend(a, lst, i);
  ar_append(a, take(lst,i+1), i+1, drop(lst,i+1), n - i - 1);
  append_take_drop(lst,i+1);
}

void release_policy_eq_low(list<char> word, list<char> guess, int i, sec lev)
  _(requires release_policy(word,guess;lev))
_(requires i :: low && i >= 0 && i < length(word))
_(ensures bang(word,i) == bang(guess,i) :: lev)
_(ensures release_policy(word,guess;lev))
_(lemma)
{
  _(unfold release_policy(word,guess;lev))
  _(fold release_policy(word,guess;lev))
}

void release_policy_contains_low(list<char> word, list<char> guess, int i, sec lev)
_(requires release_policy(word,guess;lev))
_(requires i :: low && i >= 0 && i < length(word))
_(ensures (contains(word,guess,bang(guess,i),length(word)) :: (bang(word,i) != bang(guess,i) ? lev : high)))
_(ensures release_policy(word,guess;lev))
_(lemma)
{
  _(unfold release_policy(word,guess;lev))
  _(fold release_policy(word,guess;lev))
}

void drop_cons(list<int> lst, int i, int hd, list<int> tl)
  _(requires i >= 0)
  _(requires drop(lst,i) == cons(hd,tl))
  _(ensures drop(lst,i+1) == tl)
  _(lemma) _(pure)
{
  if (i == 0){
  }else{
    if (lst == nil){
    }
    _(assert exists int hd1, list<int> tl1. lst == cons(hd1,tl1))
    drop_cons(tl1,i-1,hd,tl);
  }
}

void bang_snoc_triv(list<ans> lst, ans x, int i, int n)
_(requires n == length(lst))
_(ensures i >= 0 ==> (i < length(lst) ==> bang(snoc(x,lst),i) == bang(lst,i)))
_(lemma) _(pure)
{
  if (i >= 0 && i < n){
    if (i == 0){
      length_nonzero_cons(lst);
    }else{
      _(assert i > 0)
      length_nonzero_cons(lst);
      _(assert exists ans hd, list<ans> tl. lst == cons(hd,tl))
      _(assert length(lst) == length(tl) + 1)
      _(assert (i-1) < length(tl))        
      bang_snoc_triv(tl,x,i-1,n-1);
    }    
  }
}

void bang_snoc(list<ans> lst, ans x, int n)
_(requires length(lst) == n)
_(ensures bang(snoc(x,lst),n) == x)
_(lemma) _(pure)
{
  if (lst == nil){
    _(assert n == 0)
  }else{
    _(assert exists ans hd, list<ans> tl. lst == cons(hd,tl))
    bang_snoc(tl,x,n-1);
    length_nonnegative(tl);
    _(assert length(lst) == length(tl) + 1)
    _(assert length(tl) + 1 > 0)
  }  
}

void correct_output_extend(list<char> word, list<char> guess, list<ans> out, int i, int ot)
  _(requires correct_output(word,guess,out,i))
  _(requires ot == (bang(guess,i) == bang(word,i) ? GREEN :
                    (contains(word,guess,bang(guess,i),length(word)) ? YELLOW : GREY)))
  _(ensures correct_output(word,guess,snoc(ot,out),i+1))
  _(lemma) _(pure)
{
  _(assert length(out) == i)
  _(assume length(snoc(ot,out)) == i + 1)
  _(apply forall int k. bang_snoc_triv(out,ot,k,length(out)))
  bang_snoc(out,ot,i);
}

void ar_length(char *lst, list<char> _lst, int n)
  _(requires n :: low && n >= 0)
  _(requires ar(lst;_lst,n))
  _(ensures ar(lst;_lst,n))
  _(ensures length(_lst) == n)
  _(lemma)
{
  if (n == 0){
    _(unfold ar(lst;_lst,n))
    _(fold ar(lst;_lst,n))
  }else{
    _(assert n > 0)
    _(unfold ar(lst;_lst,n))
    _(assert exists char hd, list<char> tl. _lst == cons(hd,tl))
    ar_length(lst+1,tl,n-1);
    _(fold ar(lst;_lst,n))
  }
}

#endif // __SECC

void do_guess(char *word, char *guess, ans *out, int n)
_(requires n >= 0 && n :: low)
_(requires exists list<char> _word. ar(word;_word,n))
_(requires exists list<char> _guess. ar(guess;_guess,n))
_(requires exists list<ans> _orout. ar(out;_orout,n))
_(requires exists sec lev. release_policy(_word,_guess;lev) && lev :: low)
_(ensures ar(word;_word,n) && ar(guess;_guess,n))
_(ensures exists list<ans> _out. _out :: lev && ar(out;_out,n))
_(ensures correct_output(_word,_guess,_out,length(_out)))
_(ensures length(_out) == n)
{
  _(apply { ar_length(word,_word,n); ar_length(guess,_guess,n); ar_length(out,_orout,n); })
    
  int i=0;
  _(fold ar(out;nil,i))

  _(apply correct_output_nil(_word,_guess);)
  
  while (i < n)
    _(invariant exists list<ans> _out. i >= 0 && i <= n && i :: low &&
      /* XXX: this is super fragile because the order of the two ar
              predicates here matters. In particular when checking
              that the invariant holds initially (when i==0) they will both
              match any heap chunk ar(x,;..) in which x == out. If we swap
              their order below SecC will match them against the wrong chunks,
              causing it to say that it cannot find successful matches */
      ar(out;_out,i) && correct_output(_word,_guess,_out,i) &&
      ar(out+i;drop(_orout,i),n-i) && ar(word;_word,n) &&
      ar(guess;_guess,n) && release_policy(_word,_guess;lev))
  {
    _(apply ar_expose(guess,_guess,i,n);)
    _(apply ar_expose(word,_word,i,n);)
    char g = guess[i];
    int eq = toint(g == word[i]);
    _(apply ar_cover(word,_word,i,n);)
    _(apply ar_cover(guess,_guess,i,n);)
    _(apply release_policy_eq_low(_word,_guess,i,lev);)
    _(apply release_policy_contains_low(_word,_guess,i,lev);)
    int ot = choose_ct(eq,GREEN,choose_ct(ccontains(word,guess,n,g),YELLOW,GREY));

    _(unfold ar(out+i;drop(_orout,i),n-i))
    _(assert exists int _dorouthd, list<int> _dorouttl. drop(_orout,i) == cons(_dorouthd,_dorouttl))
    out[i] = ot;
    _(apply ar_extend(out,_out,i,ot);)
    _(apply drop_cons(_orout,i,_dorouthd,_dorouttl);)
    _(apply correct_output_extend(_word,_guess,_out,i,ot);)
    i++;
  }
  _(apply central_lemma(_word,_guess,_out,lev);)
  _(unfold ar(out + i; drop(_orout,i),n - i))
}

#if __SECC

/* Each player has a security level */
_(function sec plev(int player))
_(axioms forall int player. plev(player) :: low)

struct event {
  int player;
  list<char> guess;
};
typedef struct event ev;
_(predicate io_trace(;list<ev> tr))

_(predicate player_ready_to_guess(int player))
_(predicate player_not_ready_to_guess(int player))

/* the declassification policy */
_(function bool current_guess(int player, list<char> guess, list<ev> tr))

_(rewrites
  forall int player, list<char> guess.
  current_guess(player,guess,nil) <=> false;
  forall int player, list<char> guess, ev hd, list<ev> tl.
    current_guess(player,guess,cons(hd,tl)) ==
  (hd.player == player ?
  hd.guess == guess : current_guess(player,guess,tl)))

// make life easier by just having a static word trying to be guessed
_(predicate word_of_the_day(;list<char> word))

/* the policy for when it's safe to assume the release_policy */
_(predicate safe_to_release(list<char> word, list<char> guess, int player)
  exists list<ev> tr. io_trace(;tr) && current_guess(player,guess,tr) &&
  word_of_the_day(;word))

#endif // __SECC


void acquire_lock();
_(ensures exists list<ev> tr. io_trace(;tr))

void release_lock();
_(requires exists list<ev> tr. io_trace(;tr))

void wait_for_guess(int player);
_(requires player :: low)
_(requires player_not_ready_to_guess(player))
_(ensures player_ready_to_guess(player))
  
// get a guess from a specific player (does not block other players)
// the use of player_not_ready_to_guess ensures we need to wait for the guess
// before we call this again
char *get_guess(int player);
_(requires player :: low)
_(requires player_ready_to_guess(player))
_(requires exists list<ev> tr. io_trace(;tr))
_(ensures exists ev e. e.player == player)
_(ensures ar(result;e.guess,5) && e.guess :: plev(player))
_(ensures io_trace(;cons(e,tr)))
_(ensures player_not_ready_to_guess(player))


/* Frees the guess and answer at the same time */
void return_answer(int player, char *guess, ans *answer);
_(requires exists list<char> _word. word_of_the_day(;_word))
_(requires exists list<ans> _answer, int n. ar(answer;_answer,n) && n :: low)
_(requires exists list<ans> _guess. ar(guess;_guess,n))
_(requires _answer :: plev(player) && _guess :: plev(player))
_(requires correct_output(_word,_guess,_answer,n))
_(ensures word_of_the_day(;_word))


void run_game_for_player(int player, char *word, ans *out)
_(requires exists list<char> _word. word_of_the_day(;_word) && ar(word;_word,5))
_(requires player :: low)
_(requires player_not_ready_to_guess(player))
_(requires exists list<ans> _orout. ar(out;_orout,5))
_(ensures player_not_ready_to_guess(player))
_(ensures exists list<char> _word. word_of_the_day(;_word) && ar(word;_word,5))
{
  wait_for_guess(player);
  acquire_lock();
  char *guess = get_guess(player);
  _(assert exists list<char> _guess. ar(guess;_guess,5))
  _(assert exists list<ev> _tr. io_trace(;_tr))
  _(assert current_guess(player,_guess,_tr))
  /* need to declassify now while we have the IO trace */
  _(fold safe_to_release(_word,_guess,player))
  _(assume release_policy(_word,_guess;plev(player)))
  _(unfold safe_to_release(_word,_guess,player))
  release_lock();
  do_guess(word,guess,out,5);
  return_answer(player,guess,out);
}

  
