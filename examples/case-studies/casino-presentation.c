#include "secc.h"

// Casino Specification and Verification Challenge
// https://verifythis.github.io/casino/
// (C) 2022 Gidon Ernst

// This file presents the main ingredients of casino.c

// Contributions specific to SecC [Ernst & Murray, CAV 2019]
// - security proof
// - adherence to high-level protocol specicificaiton


// ************ Model of accounts and transfers

struct address;

_(predicate
  account(struct address *address; int balance))

_(predicate  // payment in flight
  payment(struct address *sender, struct address *receiver;  int amount))

void receive(struct address *self, struct address *sender);
  _(requires payment(sender, self; ?amount))
  _(requires account(self; ?balance))
  _(ensures  account(self; balance + amount) && amount >= 0)

bool transfer(struct address *self, struct address *receiver, int amount);
  _(requires account(self; ?balance) && balance >= amount)
  _(ensures  result :: low)
  _(ensures  result ? account(self; balance - amount) && payment(self, receiver; amount) : false)














// ************ Casino's state

_(predicate
  game(struct address *self, struct casino *casino; ...)

  // account data
  account(self; _balance) && 0 <= _balance

  // attributes: these are stored in public (low) memory locations
  && &casino->state    |->[low] _state
  && &casino->operator |->[low] _operator
  && &casino->hash     |->[low] _hash
  && &casino->player   |->[low] _player
  && &casino->guess    |->[low] _guess
  && &casino->pot      |->[low] _pot
  && &casino->bet      |->[low] _bet

  // some state invariants
  && (_state == STATE_IDLE || _state == STATE_AVAILABLE
        ==> _pot <= _balance)
  && (_state == STATE_PLACED
        ==> _player != null && _bet <= _pot && _pot + _bet <= _balance))












// ************ Example operation: adding money to the pot (security disregarded here)

void add_to_pot(struct address *self, struct address *sender, int amount, struct casino *casino)

  _(requires payment(sender, self; amount)) // payable
  _(requires game(self, casino; ?_balance, ?_state, ?_operator, ?_hash, ?_player, ?_guess, ?_pot, ?_bet))
  _(requires sender == _operator && 0 <= amount)

  _(ensures  game(self, casino;
    _balance + amount, // the actual balance is increased
    _state, _operator, _hash, _player, _guess,
    _pot     + amount, // the pot is increased
    _bet))

  // these events happen during the execution of the method
  _(trace add_to_pot)
{
  // account(self; balance) && payment(sender, self; amount)
  receive(self, sender);
  // account(self; balance + amount)

  casino->pot += amount;

  // indicate that some event has happened
  _(emit add_to_pot)
}










// ************ Security: creating the game

void create_game(struct address *self, struct address *sender, struct casino *casino, int hash)
  // all parameters are pulic
  _(requires self :: low && sender :: low && casino :: low)
  _(requires hash :: low) // notably the hash of the secret

{
  // storing into a public location requires
  // - the pointer to be public (attacker can observe memory access pattern)
  // - the value   to be public

  // &casino->hash |->[low] _  &&  casino :: low  &&  hash :: low
  casino->hash  = hash;
  // &casino->hash |->[low] hash

  // ...
}


// ************ Security: placing a bet

void place_bet(struct address *self, struct address *sender, int amount, struct casino *casino, enum coin guess)
  _(requires amount :: low && guess :: low)
  // ...

















// ************ Example operation: deciding a bet

int decide_bet(struct address *self, struct address *sender, struct casino *casino, int secret)
  _(requires game(self, casino; ?_balance, STATE_PLACED, ?_operator, ?_hash, ?_player, ?_guess, ?_pot, ?_bet))

  // secret can been released!
  _(requires secret :: low)

  // and the secret is indeed the correct one
  _(requires _hash == _cryptohash(secret))

  // information about the outcome
  _(ensures result :: low)
  _(ensures result ==> payment(self, _player; 2*_bet))
{
  // secret :: low ==> coin :: low
  enum coin coin  = (secret % 2 ? HEADS : TAILS);

  // &casino->guess |->[low] _guess ==> _guess :: low
  int player_wins = (casino->guess == coin);

  // timing sensitive security guarantee: all decisions must be public
  // coin :: low && casino->guess :: low
  //   ==> player_wins :: low
  if(player_wins) {
    // ...
  } else {
    // ...
  }
}








// ************ Security: putting it together

int decide_bet(struct address *self, struct address *sender, struct casino *casino, int secret)
  _(requires secret :: low)
  _(requires _hash == _cryptohash(secret))


void sequential_game(struct address *self, struct address *operator, struct casino *casino, int balance)
{
  int secret;
  
  while(...)
    _(invariant casino->hash == _cryptohash(secret))
  {
    struct action action = nondet_int();
    _(assume action :: low)

    if(action == CREATE_GAME && casino->state == STATE_IDLE) {
      secret = nondet_int();
      // secret :: high     ( <=> true )

      int hash = cryptohash(secret);
      create_game(self, operator, casino, hash);
    }

    if(action == DECIDE_BET && casino->state == STATE_PLACED) {
        // model operator's decision to release the secret
        _(assume secret :: low)
        decide_bet(self, operator, casino, secret);
    }
  }
}








// ************ Abstraction to traces: putting it together

void add_to_pot(struct address *self, struct address *sender, int amount, struct casino *casino)
  _(trace add_to_pot)

int decide_bet(struct address *self, struct address *sender, struct casino *casino, int secret)
  _(trace player_wins | casino_wins)

void sequential_game(struct address *self, struct address *operator, struct casino *casino, int balance)
  // regular expression to model admissible event sequences
  _(trace init
        ((add_to_pot | remove_from_pot)* create_game
            (add_to_pot | remove_from_pot)* place_bet
            add_to_pot* (player_wins | casino_wins))*)

{
  while(... && state != STATE_IDLE) // modeled here: cannot abort a game

    _(trace
        ((add_to_pot | remove_from_pot)* create_game
        (add_to_pot | remove_from_pot)* place_bet
        add_to_pot* (player_wins | casino_wins))*
            if state == STATE_IDLE)

    _(trace
        ((add_to_pot | remove_from_pot)* place_bet
        add_to_pot* (player_wins | casino_wins))

        ((add_to_pot | remove_from_pot)* create_game
        (add_to_pot | remove_from_pot)* place_bet
        add_to_pot* (player_wins | casino_wins))*
            if state == STATE_AVAILABLE)
    
    _(trace
        (add_to_pot* (player_wins | casino_wins))

        ((add_to_pot | remove_from_pot)* create_game
        (add_to_pot | remove_from_pot)* place_bet
        add_to_pot* (player_wins | casino_wins))* 
            if state == STATE_PLACED)
  { ... }
}