#include "secc.h"
/* 
This piece of text would give an understanding of SecC semicolon.
One of the major challenge in SecC is understanding semicolon to 
encode predicates. 

Mukesh : what is the purpose of semicolon. 

Gidon:
it separates the list of input parameters and output parameters.
think about p(x; y) as defining a functional dependency y = f(x)
the inputs x are used to locate the predicate in the heap. of course, it would be much better if there was an error message if they contain existentially bound variables
in a heap with p(e1; f1) and p(e2; f2) where e and f are expressions, matching exists y. p(x; y) tries to prove e1 == x and e2 == x if the first one does not suucceed
the implicit assumption is that x determines all pointers that p depends on
if say e1 == x holds, then y is instantiated to f1
you see, for two different pointers e1 and e2, we could have f1 == f2 and we should not rely on f1 or f2 to tell us which instance of p to match
in principle one could try to infer such things

When you start working with SecC, at certain point, you would encounter error like: 
 memory ⚡
    cannot find 

Protip: The Scala code responsible for this error is here:
  https://bitbucket.org/covern/secc/src/8976ba7799265b40551b9af3388c42ffb1daf16d/secc/src/secc/c/State.scala?at=master#lines-193
 and the magic lies in val (chunk, rest) = heap access (pred, in, check)  

My understanding/intuition of semicolon: when we have a parameter in a predicate, and it 
 involves some existential quantifier in the body of the predicates, then it should 
 be after semicolon. For example, the parameter ls of simp_invariant_nogood  involves
 two existential quantifiers x and xs in the body of predicate, so it should 
 be after semicolon. 

Gidon: can you attest/verify this intuition? 

TODO (to myself): 
 add this feature so that we get error if semicolon is not used properly (and most likely, 
 this document would go aways).
  
Lets take a simple predicate described below. It is not a very interesting predicate, and 
it simply connects the memory in C (tm) with logical memory (ls).
*/

_(predicate simp_invariant_nogood(int *tm, int i, int j, list<int> ls)
  (i < j ==> exists int x, list<int> xs. 
    (tm+i) |-> x && ls == cons(x, xs) &&
    simp_invariant_nogood(tm, i+1, j, xs)))

/*
To get this error: remove the _(fails memory)
In this example, when you would reach the assert in else branch, SecC would 
complain: 
error: 
cannot find
    simp_invariant_nogood(tm₁,(i₂ + 1),j₃,xs₉; ?)     
    */


void simple_lemma_can_not_find(int *tm, int i, int j, list<int> ls)
_(requires i <= j && i :: low && j :: low)
_(requires simp_invariant_nogood(tm, i, j, ls))
_(ensures simp_invariant_nogood(tm, i, j, ls))    
_(lemma) _(fails memory)
{

  if(i == j)
  {
    _(unfold simp_invariant_nogood(tm, i, i, ls))
    _(fold simp_invariant_nogood(tm, i, i, ls))
  }
  else
  {
     _(assert i < j)
     _(unfold simp_invariant_nogood(tm, i, j, ls))
     _(assert exists int y, list<int> ys. 
        ls == cons(y, ys) &&  (tm+i) |-> y &&
        simp_invariant_nogood(tm, i+1, j, ys))
     simple_lemma(tm, i+1, j, ys);
     _(assert simp_invariant_nogood(tm, i+1, j, ys))
     _(fold simp_invariant_nogood(tm, i, j, ls))
  }
}


/*
Now lets modify the predicate and make ls appear after a semicolon. 

ls = fn(tm, i, j) ?
 */

_(predicate simp_invariant(int *tm, int i, int j; list<int> ls)
  (i < j ==> exists int x, list<int> xs. 
    ls == cons(x, xs) &&  (tm+i) |-> x &&
    simp_invariant(tm, i+1, j; xs)))


/*
And lo and behold, SecC is super happy */

void simple_lemma(int *tm, int i, int j, list<int> ls)
_(requires i <= j && i :: low && j :: low)
_(requires simp_invariant(tm, i, j; ls))
_(ensures simp_invariant(tm, i, j; ls))    
_(lemma)
{

  if(i == j)
  {
    _(unfold simp_invariant(tm, i, i; ls))
    _(fold simp_invariant(tm, i, i; ls))
  }
  else
  {
     _(assert i < j)
     _(unfold simp_invariant(tm, i, j; ls))
     _(assert exists int y, list<int> ys. 
        ls == cons(y, ys) &&  (tm+i) |-> y &&
        simp_invariant(tm, i+1, j; ys))
     simple_lemma(tm, i+1, j, ys);
     _(assert simp_invariant(tm, i+1, j; ys))
     _(fold simp_invariant(tm, i, j; ls))
  }
}


