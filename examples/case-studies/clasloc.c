#include "secc.h"

/* a classified location */
typedef struct {
  int clas; // 0 is low, 1 is high
  int val;
  int full; // boolean
} clasloc_t;

_(predicate clasloc_inv(;clasloc_t *p, int _clas, int _val, int _full)
  &p->clas |-> _clas && &p->val |-> _val && &p->full |-> _full &&
  _full :: low &&
  (_full ==> (_clas :: low && _val :: (_clas ? high : low))))

int put(clasloc_t *p, int clas, int val)
  _(requires exists int _clas, int _val, int _full. clasloc_inv(;p,_clas,_val,_full))
  _(requires clas :: low && val :: (clas ? high : low))
  _(ensures _full :: low)
  _(ensures _full ==> result == 1 && clasloc_inv(;p,_clas,_val,_full))
  _(ensures !_full ==> result == 0 && clasloc_inv(;p, clas, val, 1))
{
  _(unfold clasloc_inv(;p,_clas,_val,_full))
  if (!p->full){
    p->val = val;
    p->clas = clas;
    p->full = 1;
    _(fold clasloc_inv(;p,clas,val,1))
    return 0;
  } else {
    _(fold clasloc_inv(;p,_clas,_val,_full))
    return 1;
  }   
}

int get(clasloc_t *p, int *clas, int *val)
  _(requires exists int _clas, int _val, int _full. clasloc_inv(;p,_clas,_val,_full))
  _(requires exists int _c, int _v. clas |-> _c && val |-> _v)
  _(ensures _full :: low)
  _(ensures !_full ==> result == 1 && clasloc_inv(;p,_clas,_val,_full) && clas |-> _c && val |-> _v)
  _(ensures _full ==> result == 0 && clasloc_inv(;p,_clas,_val,0) && clas |-> _clas && val |-> _val && _clas :: low && _val :: (_clas ? high : low))
{
  _(unfold clasloc_inv(;p,_clas,_val,_full))
  if (p->full){
    *val = p->val;
    *clas = p->clas;
    p->full = 0;
    _(fold clasloc_inv(;p,_clas,_val,0))
    return 0;
  } else {
    _(fold clasloc_inv(;p,_clas,_val,_full))
    return 1;
  }
}

clasloc_t *g_p;

void acquire_lock();
_(ensures exists int _c, int _v, int _f. clasloc_inv(;g_p,_c,_v,_f))

void release_lock();
_(requires exists int _c, int _v, int _f. clasloc_inv(;g_p,_c,_v,_f))

int rand();
_(ensures (result % 2) :: low && result :: ((result % 2) ? high : low))


void thread1(){
  int res;
  int c;
  int v;
  int done;
  while(1) {
    v = rand();
    c = v % 2;
    done = 0;
    while(!done) _(invariant done :: low) {
      acquire_lock();
      res = put(g_p,c,v);
      release_lock();
      if (res == 0){
        done = 1;
      }
    }
  }
}

int *resc;
int *resv;

void start_timer();
_(requires true)
_(ensures true)

void log_count(int count);
_(requires count :: low)
_(ensures true)

void thread2()
  _(requires exists int _resc, int _resv. resc |-> _resc && resv |-> _resv)
{
  int res;
  int done;
  int count = 0;
  start_timer();
  while(1)
    _(invariant exists int _resc, int _resv. resc |-> _resc && resv |-> _resv && count :: low)
  {
    done = 0;
    while(!done) _(invariant done :: low &&
                   exists int _resc, int _resv. resc |-> _resc && resv |-> _resv &&
                   (done ==> _resc :: low && (_resv :: (_resc ? high : low)))){
      acquire_lock();
      res = get(g_p,resc,resv);
      release_lock();
      if (res == 0){
        done = 1;
      }
    }
    _(assert exists int _resc, int _resv. resc |-> _resc && resv |-> _resv && _resc :: low && (_resv :: (_resc ? high : low)))
    count++;
    if (count % (1024*128) == 0){
      log_count(count);
    }
  }
}
