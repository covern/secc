#include "secc.h"

enum auction_status {
  Ready, 
  Ongoing, 
  Closed
};

struct bid {
  int id;     // bidder identity
  int quote;  // bid amount
};

struct auction {
  enum auction_status status;

  // identity and quote of the higghest bidder
  int id;
  int quote;
};

enum event_type {
  Running, 
  Finished
};

struct event {
  enum event_type type; 
  int id;
  int quote;
};

_(predicate io_trace(; list<struct event> io))

_(predicate memory_inv(struct auction *auction; enum auction_status status, int id, int qt)
  auction :: low  && status :: low &&
  (status == Ready || status == Ongoing || status == Closed) &&
    &auction->status |-> status &&
    &auction->id     |-> id &&
    &auction->quote  |-> qt)


_(function bool contains(struct event e, list<struct event> io))
_(axioms
  forall struct event e.
    contains(e, nil) <=> false;
  forall struct event e, struct event f, list<struct event> io.
    contains(e, cons(f, io)) <=> e == f || contains(e, io))

_(predicate running_auction(; int id, int qt, list<struct event> io)
    (forall struct event f. contains(f, io) ==> f.type == Running && f.quote <= qt) &&
    (exists struct event f. contains(f, io) && f.id == id && f.quote == qt))

struct event log_bid(int id, int qt);
    _(requires exists list<struct event> io.
        io_trace(; io))
    _(ensures result.id == id && result.quote == qt && result.type == Running &&
        io_trace(; cons(result, io)))

struct event log_close(int id, int qt);
    _(requires exists list<struct event> ios.
        io_trace(; io))
    _(ensures result.id == id && result.quote == qt && result.type == Finished &&
        io_trace(; cons(result, io)))

int to_int(bool x);
    _(ensures (bool)((!x ==> result == 0)  && (x ==> result == 1)))

int select_max(bool x, int a, int b)
    _(ensures (bool)((!x ==> result == b) && (x ==> result == a)))
{
    int t = to_int(x);
    int c = t * a + (1-t) * b; 
    return c;
}

void register_first_bid(int bid_id, int bid_quote, struct auction *auction)
    _(requires exists int id, int qt.
        memory_inv(auction; Ready, id, qt))
    _(ensures
        memory_inv(auction; Ongoing, bid_id, bid_quote))

    _(requires
        io_trace(; nil))
    _(ensures exists struct event e.
        e.type == Running && e.id == bid_id && e.quote == bid_quote &&
        io_trace(; cons(e, nil)))

    _(ensures  running_auction(; bid_id, bid_quote, cons(e, nil)))
{
    _(unfold memory_inv(auction; Ready, id, qt))

    struct event e = log_bid(bid_id, bid_quote);

    auction->status = Ongoing;
    auction->id    = bid_id;
    auction->quote = bid_quote;

    _(fold memory_inv(auction; Ongoing, bid_id, bid_quote))
    _(fold running_auction(; bid_id,  bid_quote, cons(e, nil)))
}

void register_bid(int bid_id, int bid_quote, struct auction *auction)
    _(requires exists int id, int qt.
        memory_inv(auction; Ongoing, id, qt))
    _(ensures  exists int id_, int qt_.
        memory_inv(auction; Ongoing, id_, qt_))

    _(requires exists list<struct event> io.
        io_trace(; io))
    _(ensures exists struct event e.
        e.type == Running && e.id == bid_id && e.quote == bid_quote &&
        io_trace(; cons(e, io)))

    _(requires running_auction(; id, qt, io))
    _(ensures  running_auction(; id_, qt_, cons(e, io)))
{
    _(unfold memory_inv(auction; Ongoing, id, qt))
    _(unfold running_auction(; id, qt, io))

    struct event e = log_bid(bid_id, bid_quote);

    bool b   = bid_quote > auction->quote;
    int  id_ = select_max(b, bid_id,    auction->id);
    int  qt_ = select_max(b, bid_quote, auction->quote);
    auction->id    = id_;
    auction->quote = qt_;

    _(fold memory_inv(auction; Ongoing, id_, qt_))
    _(fold running_auction(; id_,  qt_, cons(e, io)))
}

_(predicate safe_to_declassify_bid(int id, int qt, struct event e, list<struct event> io)
    io_trace(; cons(e, io)) &&
    e.type == Finished && e.id == id && e.quote == qt &&
    running_auction(; id, qt, io))

void print_winner(int id, int qt);
    _(requires id :: low)
    _(requires qt :: low)

void close_auction(struct auction *auction)
    _(requires exists int id, int qt.
        memory_inv(auction; Ongoing, id, qt))
    _(ensures
        memory_inv(auction; Closed, id, qt))

    _(requires exists list<struct event> io.
        io_trace(; io))
    _(ensures exists list<struct event> io_.
        io_trace(; io_))

    _(requires running_auction(; id,  qt, io))
{
    _(unfold memory_inv(auction; Ongoing, id, qt))

    auction->status = Closed;

    int id    = auction->id;
    int quote = auction->quote;
    struct event e = log_close(id, quote);

    _(assert io_trace(; cons(e, io)))

    _(assert e.type == Finished && e.id == id && e.quote == qt)
    _(assert running_auction(; id, qt, io))
    _(assume id :: low && quote :: low)
    print_winner(id, quote);

    // discard this information, because
    // the auction is not running any longer
    _(unfold running_auction(; id,  qt, io))

    _(fold memory_inv(auction; Closed, id, qt))
}