enum answer {
  GREY,
  YELLOW,
  GREEN
};

typedef enum answer answer;


_(function
  bool contains(char c, map<int,char> word, int n))
_(function
  bool contains_elsewhere(char c, map<int,char> word, map<int,char> guess, int i, int n))
_(function
  bool correct_output_at(map<int,char> word, map<int,char> guess, map<int,answer> out, int i, int n))

_(axioms
  forall char c, map<int,char> word, int n.
    contains(c, word, n)
      == exists int k. 0 <= k && k < n && c == word[k];
  forall char c, map<int,char> guess, map<int,char> word, int i, int n.
    contains_elsewhere(c, word, guess, i, n)
      == exists int j. i <= j && j < n && c == word[j] && guess[j] != word[j];
  forall map<int,char> word, map<int,char> guess, map<int,answer> out, int i, int n.
    correct_output_at(word, guess, out, i, n)
      ==   (out[i] == GREEN  <=> guess[i] == word[i])
        && (out[i] == YELLOW <=> guess[i] != word[i] &&  contains_elsewhere(guess[i], word, guess, 0, n))
        && (out[i] == GREY   <=> guess[i] != word[i] && !contains_elsewhere(guess[i], word, guess, 0, n)))

int choose_ct(int c, int a, int b)
    _(requires c == 0 || c == 1)
    _(ensures  (bool)((c == 1 ==> result == a) && (c == 0 ==> result == b)))
{
    return c * a + (1-c) * b; 
}

int ccontains(char c, char *word, char *guess, int i, int n)
  _(requires i :: low && n :: low)
  _(requires 0 <= i && i <= n)
  _(requires exists map<int,char> _word, map<int,char> _guess.
    word  |=>[n] _word &&
    guess |=>[n] _guess)
  _(ensures
    word  |=>[n] _word &&
    guess |=>[n] _guess)

  _(ensures result == 0 || result == 1) // DO KEEP THIS CONDITION, booleans are slightly broken (i.e. unsound)
  _(ensures result <=> contains_elsewhere(c, _word, _guess, i, n))
  // _(ensures result <=> exists int j. i <= j && j < n && c == _word[j] && _guess[j] != _word[j])
{
  if (i < n) {
    int b = ccontains(c, word, guess, i+1, n);
    /* technically we are allowed to branch here but it's easier to avoid it
       to save having to convince the prover we are allowed to */
    int d = choose_ct((int)(word[i] == guess[i]), 0, (int)(c == word[i]));
    int c = choose_ct(d,1,b);

    return c;
  } else {
    return 0;
  }
}

_(predicate
  correct_output(map<int,char> word, map<int,char> guess, map<int,answer> out, int n)
    forall int i.
      0 <= i && i < n
        ==> correct_output_at(word, guess, out, i, n))

void correct_output_at_preserve(map<int,char> _word, map<int,char> _guess, map<int,answer> _out, int k, int i, int o)
  _(requires 0 <= k && k < i)
  _(requires correct_output_at(_word, _guess, _out, k, i))
  _(ensures  correct_output_at(_word, _guess, _out[i:=o], k, i+1))
  _(pure) _(lemma)
{}

void correct_output_extend(map<int,char> _word, map<int,char> _guess, map<int,answer> _out, int i, int o)
  _(requires correct_output_at(_word, _guess, _out[i:=o], i, i+1))
  _(requires correct_output(_word, _guess, _out, i))
  _(ensures correct_output(_word, _guess, _out[i:=o], i+1))
  _(lemma)
{
  // wow, this is brittle!
  _(unfold correct_output(_word, _guess, _out, i))
  _(apply  forall int k. correct_output_at_preserve(_word, _guess, _out, k, i, o))
  _(fold   correct_output(_word, _guess, _out[i:=o], i+1))
}

_(predicate release_policy(map<int,char> word, map<int,char> guess, int n, sec lev)
  forall int i.
    i :: low && 0 <= i && i < n
      ==>    word[i] == guess[i] :: lev
          && contains_elsewhere(guess[i], word, guess, 0, n)
              :: (word[i] != guess[i] ? lev : high))


void central_lemma(map<int,char> word, map<int,char> guess, map<int,answer> out, int n, sec lev)
  _(requires 0 <= n && n :: low && lev :: low)
  _(requires correct_output(word, guess, out, n))
  _(requires release_policy(word, guess, n, lev))
  _(ensures  forall int i. 0 <= i && i < n && i :: low ==> out[i] :: lev)
  _(lemma)
{
  _(unfold correct_output(word, guess, out, n))
  _(unfold release_policy(word, guess, n, lev))

  int i = 0;
  while(i < n)
    _(invariant 0 <= i && i <= n && i :: low)
    _(invariant forall int k. 0 <= k && k < i && k :: low ==> out[k] :: lev)
  {
    _(assert correct_output_at(word, guess, out, i, n)) // this is needed for triggering
    _(assert out[i] :: lev)
    i++;
  }
}

void output_init(map<int,char> _word, map<int,char> _guess, map<int,char> _out)
  _(ensures correct_output(_word, _guess, _out, 0))
{
  _(fold correct_output(_word, _guess, _out, 0))
}

int output(char *word, char *guess, char *out, int i, int n)
  _(requires i :: low && n :: low)
  _(requires 0 <= i && i <= n)
  _(requires exists map<int,char> _word, map<int,char> _guess, map<int,char> _out.
    word  |=>[n] _word  &&
    guess |=>[n] _guess &&
    out   |=>[n] _out)
  _(ensures exists map<int,char> _out.
    word  |=>[n] _word  &&
    guess |=>[n] _guess &&
    out   |=>[n] _out)
  _(requires correct_output(_word, _guess, _out, i))
  _(ensures  correct_output(_word, _guess, _out, n))
{
  if (i < n) {
    char c  = guess[i];
    int  eq = (int)(guess[i] == word[i]);
    int  r  = ccontains(c, word, guess, 0, n);

    int  o  = choose_ct(eq, GREEN, choose_ct(r, YELLOW, GREY));

    out[i] = o;

    // Note: placed into separate lemma because this proof is brittle
    _(apply correct_output_extend(_word, _guess, _out, i, o);)

    output(word, guess, out, i+1, n);
  } else {
  }
}