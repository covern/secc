// Password checking case study

// Equality of functional array ranges is not built-in
_(function bool eq(map<int,int> a, map<int,int> b, int n))
_(axioms forall map<int,int> a, map<int,int> b, int n.
    eq(a, b, n) == forall int k. 0 <= k && k < n ==> a[k] == b[k])

// Constant time if-then-else, realized by multiplication
int choose_ct(int c, int a, int b)
    _(requires c == 0 || c == 1)
    _(ensures  (bool)((c == 1 ==> result == a) && (c == 0 ==> result == b)))
{
    return c * a + (1-c) * b;
}

int check_password(int *in, int *pw, int n)
    _(requires 0 <= n && n :: low)
    _(requires exists map<int,int> _in. in |=>[n] _in && _in :: low)
    _(requires exists map<int,int> _pw. pw |=>[n] _pw)

    _(ensures  in |=>[n] _in && _in :: low)
    _(ensures  pw |=>[n] _pw)
    _(ensures  result == (int)eq(_in, _pw, n))

    // This precondition can be seen to reflect a declassification policy,
    // i.e., this function is predicated on the decision that it is ok to release whether the passwords match entirely.
    // Removing this condition here would of course void the postcondition below,
    // and instead defer the declassification to a caller.
    // Nevertheless, we include it here, to show that even in its presence,
    // we cannot prove that ok :: low inside the loop below.
    _(requires eq(_in, _pw, n) :: low)
    // It ensues therefore that the result of this function is low, too.
    _(ensures  result :: low)
{
    int i  = 0;
    int ok = 1;

    while(i < n)
        _(invariant 0 <= i && i <= n)
        _(invariant i :: low && n :: low)
        _(invariant in |=>[n] _in && _in :: low)
        _(invariant pw |=>[n] _pw)
        _(invariant ok == (int)eq(_in, _pw, i))
    {
        // This conditional is insecure:
        // - exiting the loop early affects timing, but loop guard can be maintained as low
        // - branching may affect timing, notably exiting the loop at times that are not predictable by the attacker
        // - we are allowed not allowed to release individual comparisons
        // - we are only allowed to release whether the *entire* password matches
        // if(pw[i] != in[i])
        //     return 0;

        // Instead we use a constant-time update (cf. above)
        ok = choose_ct((int)(pw[i] == in[i]), ok, 0);

        // Note, the value of ok cannot be proven public as desired,
        // even with the assumption from the precondition
        // that the checking the entire password can be released.
        // _(assert ok :: low)

        i++;
    }

    return ok;
}