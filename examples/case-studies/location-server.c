#include "secc.h"

#define NULL 0

#if __SECC
typedef int double;
#endif

/* 
This linked list stores the declassified
location of a user. The reason for keeping 
all the declassified location is that we 
use them in extrapolating a new location when 
the user does not have enough privacy budget. 
*/
struct ulocation
{
  double lat; 
  double lon;
  struct ulocation *nxt;
};


/* location modelled as a struct */
struct point
{
  double lat;
  double lon;
};


/* 
This enum represents event status, i.e. 
if it Consumed (some location has been released to the public), 
else privacy budget has been replenished after a given period 
of time. 
*/
enum ev_status
{
  Consumed,
  Replenished
};
typedef enum ev_status event_status;


/*
Things we record as a history (IO traces). 
We record the event status, noisy latitude 
(original latitdue + noise), and noisy 
longitude (original longitude + noise). This is 
the minimum amount of information we need to 
declassify a location (depending on system modelling,
it may need more or less amount of information) 
*/
struct event
{
  event_status est;
  double noislat; // noise added to real latitude 
  double noislon; // noise added to real longitude
};

/*
This enum represents if a function failed or succeed
*/
enum ret_code 
{
  Failure,
  Success
};
typedef enum ret_code status_code;

/* 
Input-Output history modelled as list, and this 
list contains enough information (snap shot of various
variables during the program execution) to ensure 
the declassification. 
*/
_(predicate io_trace(;list<struct event> xs))

/* 
Logical function (this does not compute) for appending two lists
*/
_(function list<struct event> append(list<struct event> xs, list<struct event> ys))
_(rewrites
	forall list<struct event> ys. append(nil, ys) == ys;
	forall struct event x, list<struct event> xs, list<struct event> ys.
		append(cons(x, xs), ys) == cons(x, append(xs, ys)))

      
/* 
This (logical) function compute the total budget at any point of the time. It starts 
traversing the given IO history (ios) at that point of the time, and for every record in the ios, if the 
event is Consumed, it subtracts the privacy budget (-eps), and if the event is Replenished, 
it adds the total budget (refvalue). 

The idea is that if at any point, someone is trying to release a point, the 
she has to ensure that this function returns a values greater than or equal to (>=)
eps (minimum privacy budget to generate the noise)
*/

_(function double count_total_budget(double eps, double refvalue, list<struct event> ios))
_(rewrites 
    forall double eps, double refvalue. count_total_budget(eps, refvalue, nil) == 0;
    forall double eps, double refvalue, struct event x, list<struct event> xs.
      count_total_budget(eps, refvalue, cons(x, xs)) == 
      ((x.est == Consumed) ? (-eps + count_total_budget(eps, refvalue, xs)) : 
        (refvalue + count_total_budget(eps, refvalue, xs))))

/* 
This (logical) function check if an element p is in the list ios or not. 
If the element p is in the list, it returns true else returns false. 
Notice that these "true" and "false" values are logical, and not 
computational. 
*/
_(function bool member(struct event p, list<struct event> ios))
_(rewrites 
    forall struct event p. member(p, nil) <=> false;
    forall struct event p, struct event x, list<struct event> xs. 
    (member(p, cons(x, xs)) <=> ((p == x) || member(p, xs))))

/*
Todo: Explain the difference between _(function ) and _(predicate). 

This predicate encodes the condition for releasing/declassifying a point (noislat and noislon) safely. 
Roughly, it translates 
to: if we want to release/declassify the noisy latitute (noislat = original latitude + noise) and 
noisy longitude, then we need to ensure that this point (noislat and noislon) has been recorded 
in the past in the program history (ios). Moreover, at that time there was enough budget (>= eps,
minimum privacy budget) to generate Laplacian noise to ensure that programmer has not done 
anything dubious 
*/
_(predicate safe_to_release_point(double noislat, double noislon,  double eps, double refvalue; list<struct event> iost)
      (exists list<struct event> ios, struct event e. iost == append(ios, cons(e, nil)) &&
       io_trace(;iost) &&
       e.est == Consumed && e.noislat == noislat && e.noislon == noislon && 
       count_total_budget(eps, refvalue, ios) >= eps))


/* 
Invariant for the many C functions. This, roughly, says that s, represents the database, is low, i.e. attacker 
is allowed to observe the content of this database. In addition, every entry in this database contains 
a low values (vlat :: low && vlon :: low). More importantly, we ensure that
these entries are generated in the past and recorded in the program history (ios) by looking for 
an event p in ios which specifically records this data 
*/
_(predicate location_server_inv(double eps, double refvalue; struct ulocation *s, list<struct event> ios)
  (s :: low) && 
  (s != null ==> exists double vlat, double vlon, struct event p,
    struct ulocation *next.   
    &s->lat |-> vlat && (vlat :: low) &&
    &s->lon |-> vlon && (vlon :: low) &&
    (member(p, ios) <=> true) &&
    p.est == Consumed && p.noislat == vlat && p.noislon == vlon &&
    &s->nxt |-> next && &s->nxt :: low &&
    location_server_inv(eps, refvalue; next, ios)))
 

/* 
This predicate connects the budget from the computational function to the 
logical function to ensure the correctness 
*/
_(predicate budget_and_trace_inv(double budget, double eps, double refvalue; list<struct event> ios)
    budget == count_total_budget(eps, refvalue, ios))


/* this function returns the user's real (secret) location. */
void get_real_location(struct point *p);
_(requires p :: low)
_(requires exists double olat, double olon. &p->lat |-> olat && &p->lon |-> olon)
_(ensures exists double nlat, double nlon. &p->lat |-> nlat && &p->lon |-> nlon)
_(ensures nlat :: high && nlon :: high)

/* this function adds noise to a real location of the user, in-place,
   and remembers that the noise was added in the io trace */
struct event add_noise_to_real_point(struct point *p, double eps);
_(requires p :: low)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires exists double olat, double olon. &p->lat |-> olat && &p->lon |-> olon)
_(ensures exists double nlat, double nlon. &p->lat |-> nlat && &p->lon |-> nlon)
_(ensures exists double noise_one, double noise_two. nlat == olat + noise_one &&
  nlon == olon + noise_two)
_(ensures  result.est == Consumed && result.noislat == nlat && 
  result.noislon == nlon)
_(ensures io_trace(;append(ios, cons(result, nil))))


/* this function is used to record when the budget is replenished */
struct event generate_replenished_event();
_(requires exists list<struct event> ios. io_trace(;ios))
_(ensures result.est == Replenished)
_(ensures io_trace(;append(ios, cons(result, nil))))

#if __SECC
/* 
membership extension member_snoc. 
if p is member of ios, the p is also member of (ios ++ [e])
*/ 
void member_snoc(struct event e, struct event p, list<struct event> ios)
_(requires member(p, ios) <=> true)
_(ensures member(p, append(ios, cons(e, nil))) <=> true)
_(lemma) _(pure)
{
  
 if(ios == nil)
 {
   _(assert member(p, nil) <=> false)
   _(assert false)
 }
 else
 {
   _(assert exists struct event x, list<struct event> xs. ios == cons(x, xs))
   _(assert member(p, cons(x, xs)) <=> ((p == x) || member(p, xs)))

   if(p == x)
   {
     return;
   }
   else
   {
     _(assert member(p, xs) <=> true)
     member_snoc(e, p, xs);
   }
 }
}

/* 
e is always member of list ios ++ [e]
*/
void membership_endlist(struct event e, list<struct event> ios)
_(ensures member(e, append(ios, cons(e, nil))) <=> true)
_(lemma) _(pure)
{
  if (ios != nil)
  {
    _(assert exists struct event x, list<struct event> xs. ios == cons(x, xs))
    _(assert member(e, cons(x, xs)) <=> ((e == x) || member(e, xs)))
    membership_endlist(e, xs);
  }
}

/*
Location server invariant, location_server_inv, is preserved during the addition of 
a new event, e, into the program history, ios. 
*/
void replenished_preserve_inv(struct ulocation *s, double eps, double refvalue, struct event e, list<struct event> ios)
_(requires 0 < eps)
_(requires location_server_inv(eps, refvalue; s, ios))
_(ensures location_server_inv(eps, refvalue; s, append(ios, cons(e, nil))))
_(lemma)
{

 _(unfold location_server_inv(eps, refvalue; s, ios))
 _(assert s == null :: low)
 if(s == null)
 {
  _(fold location_server_inv(eps, refvalue; s, append(ios, cons(e, nil))))
 }
 else
 {
   _(assert s != null)
   _(assert exists double vlat, double vlon, struct event p, 
      struct ulocation *next.    
      &s->lat |-> vlat && (vlat :: low) &&
      &s->lon |-> vlon && (vlon :: low) && 
      (member(p, ios) <=> true) &&
      p.est == Consumed && p.noislat == vlat && p.noislon == vlon &&
      &s->nxt |-> next && location_server_inv(eps, refvalue; next, ios))
   replenished_preserve_inv(next, eps, refvalue, e, ios);   
   _(apply member_snoc(e, p, ios);)
   _(assert member(p, append(ios, cons(e, nil))) <=> true)
   _(fold location_server_inv( eps, refvalue; s, append(ios, cons(e, nil))))
 }
      
}


/* 
Budget computed from two a list, ls ++ rs, is same as budget computed 
from ls and rs sepately and adding them 
*/
void budget_append(double eps, double refvalue, list<struct event> ls, list<struct event> rs)
_(ensures count_total_budget(eps, refvalue, append(ls, rs)) == 
    (count_total_budget(eps, refvalue, ls) + count_total_budget(eps, refvalue, rs)))
_(lemma) _(pure)
{
  if(ls != nil)
  {
    _(assert exists struct event x, list<struct event> xs. ls == cons(x, xs))
    budget_append(eps, refvalue, xs, rs);
  }
}


/*
When we Replenish the budget, then the new budget (obudget+refvalue) is refvalue more than the old budget (obudget)
*/
void adding_replenish_to_trace(double obudget, double eps, double refvalue, struct event e, list<struct event> ios)
_(requires e.est == Replenished)
_(requires budget_and_trace_inv(obudget, eps, refvalue; ios))
_(ensures budget_and_trace_inv(obudget+refvalue, eps, refvalue; append(ios, cons(e, nil))))
_(lemma)
{
  _(unfold budget_and_trace_inv(obudget, eps, refvalue; ios))
  _(assert obudget == count_total_budget(eps, refvalue, ios))
  _(assert count_total_budget(eps, refvalue, cons(e, nil)) == refvalue)
  _(apply budget_append(eps, refvalue, ios, cons(e, nil));)
  _(assert count_total_budget(eps, refvalue, append(ios, cons(e, nil))) == 
      (count_total_budget(eps, refvalue, ios) + count_total_budget(eps, refvalue, cons(e, nil))))
  _(fold budget_and_trace_inv(obudget + refvalue, eps, refvalue; append(ios, cons(e, nil))))
}

/*
Bootstrapping proof: we ensure that invariant holds when the database in empty, i.e. 
no point has been added to the database
*/
void database_proof(struct ulocation *s, double *budget, double eps, double refvalue)
_(requires  0 < eps)
_(requires s == null && s == null :: low)
_(ensures location_server_inv(eps, refvalue; s, nil))
_(lemma)
{
  _(fold location_server_inv(eps, refvalue; s, nil))
}

#endif

/* This function marks the beginning of the process. 
   It will be called whenever the server is reset.
   (Note: freeing allocated memory is the client's responsibility) */
status_code start_the_location_server(struct ulocation *s, double *budget, double eps, double refvalue)
_(requires budget :: low)
_(requires io_trace(;nil))
_(requires  0 < eps)
_(requires s == null)
_(requires exists double val. budget |-> val)
_(ensures result == Success)
_(ensures budget |-> 0)
_(ensures location_server_inv(eps, refvalue; s, nil))
_(ensures budget_and_trace_inv(0, eps, refvalue; nil))
_(ensures io_trace(;nil))
{
  *budget = 0;
  _(apply database_proof(s, budget, eps, refvalue);)
  _(assert location_server_inv(eps, refvalue; s, nil))
  _(assert 0 == count_total_budget(eps, refvalue, nil))
  _(fold budget_and_trace_inv(0, eps, refvalue; nil))
  return Success;
}


/* 
This function is called (e.g. periodically) to replenish the budget.
Notice the budget_and_trace_inv invariant. 
 */
status_code refill_budget(struct ulocation *s, double *budget, double eps, double refvalue)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires 0 < eps)
_(requires location_server_inv(eps, refvalue; s, ios))
_(requires budget :: low)
_(requires exists double obudget. budget |-> obudget)
_(requires budget_and_trace_inv(obudget, eps, refvalue; ios))
_(ensures result == Success)
_(ensures budget |-> obudget + refvalue) 
_(ensures exists struct event e. e.est == Replenished)
_(ensures exists list<struct event> iost. iost == append(ios, cons(e, nil)) && io_trace(;iost))
_(ensures location_server_inv(eps, refvalue; s, iost))
_(ensures budget_and_trace_inv(obudget + refvalue, eps, refvalue; iost))
{
  *budget = *budget + refvalue;
  struct event e = generate_replenished_event();
  _(assert e.est == Replenished)
  _(assert io_trace(;append(ios, cons(e, nil))))
  _(apply replenished_preserve_inv(s, eps, refvalue, e, ios);)
  _(apply adding_replenish_to_trace(obudget, eps, refvalue, e, ios);)
  return Success;
}



/* This function attempts to release the user's real location, after
   adding noise to it, if there is sufficient privacy budget remaining. 
   Adding noise makes the location safe to release, and so at this point it
   is safely declassified, making the released location public (low).
   Otherwise, if there is not sufficient budget remaining, this function
   releases nothing and returns failure. */
status_code release_current_location(struct ulocation *s, double *budget, double eps, double refvalue, struct point *p, struct ulocation *ncell)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires 0 < eps && eps :: low)
_(requires budget :: low && p :: low && ncell :: low)
_(requires exists double obudget. budget |-> obudget && obudget :: low)
_(requires exists double oplat, double oplon. &p->lat |-> oplat && &p->lon |-> oplon)
_(requires ncell != null)    
_(requires exists double nclat, double nclon, struct ulocation *next. &ncell->lat |-> nclat &&
    &ncell->lon |-> nclon && &ncell->nxt |-> next)
_(requires location_server_inv(eps, refvalue; s, ios))
_(requires budget_and_trace_inv(obudget, eps, refvalue; ios))
_(ensures result :: low)     
_(ensures result == Success || result == Failure)
_(ensures exists list<struct event> iost. io_trace(;iost))
_(ensures exists double nbudget. budget |-> nbudget && nbudget :: low)
_(ensures result == Success ==> exists double nplat, double nplon, struct event e. 
    &p->lat |-> nplat && &p->lon |-> nplon && nplat :: low && nplon :: low &&
    e.est == Consumed && e.noislat == nplat && e.noislon == nplon &&
    iost == append(ios, cons(e, nil)))
_(ensures result == Success ==> nbudget == (obudget - eps))
_(ensures result == Failure ==> &p->lat |-> oplat && &p->lon |-> oplon)
_(ensures result == Failure ==> nbudget == obudget)
_(ensures result == Failure ==> iost == ios)
_(ensures result == Failure ==> &ncell->lat |-> nclat && 
    &ncell->lon |-> nclon && &ncell->nxt |-> next)
_(ensures result == Success ==> location_server_inv(eps, refvalue; ncell, iost))
_(ensures result == Failure ==> location_server_inv(eps, refvalue; s, iost))
_(ensures budget_and_trace_inv(nbudget, eps, refvalue; iost))
_(ensures result == Success <=> eps <= obudget)
{
  double cbudget = *budget;
  if(cbudget >= eps) 
  {  
        _(assert location_server_inv(eps, refvalue; s, ios))

        get_real_location(p);
        _(assert exists double nlat, double nlon. &p->lat |-> nlat && &p->lon |-> nlon)
        _(assert nlat :: high && nlon :: high)
        _(assert exists double olat, double olon. &p->lat |-> olat && &p->lon |-> olon)
        
        struct event e = add_noise_to_real_point(p, eps);
        
        _(assert exists double nlat, double nlon. &p->lat |-> nlat && &p->lon |-> nlon)
        _(assert exists double noise_one, double noise_two. (nlat == olat + noise_one) &&
            (nlon == olon + noise_two))
        _(assert e.est == Consumed && e.noislat == nlat && 
            e.noislon == nlon)
        _(assert io_trace(;append(ios, cons(e, nil))))
        
        double nlat = p->lat; 
        double nlon = p->lon; 
        
        _(assert exists list<struct event> iost. iost == append(ios, cons(e, nil)) && 
          io_trace(;iost)) 
        _(apply replenished_preserve_inv(s, eps, refvalue, e, ios);)
        _(assert location_server_inv(eps, refvalue; s, iost))
        
      
        _(unfold budget_and_trace_inv(obudget, eps, refvalue; ios))
        _(assert cbudget == count_total_budget(eps, refvalue, ios))
        _(assert io_trace(;iost) && count_total_budget(eps, refvalue, ios) >= eps)
        /* this part has to be checked manually to ensure that assume is used correctly */
        // _(fold safe_to_release_point(nlat, nlon, eps, refvalue; iost))

        _(assert
          io_trace(;iost) &&
          e.est == Consumed && e.noislat == nlat && e.noislon == nlon && 
          count_total_budget(eps, refvalue, ios) >= eps)

        _(assume nlat :: low && nlon :: low) //safely release the latitude and the longitude
        /* end of manual checking */
        // _(unfold safe_to_release_point(nlat, nlon, eps, refvalue; iost))

        *budget = cbudget - eps;
        _(apply budget_append(eps, refvalue, ios, cons(e, nil));)
        _(assert count_total_budget(eps, refvalue, cons(e, nil)) == (-eps)) 
        _(assert count_total_budget(eps, refvalue, append(ios, cons(e, nil))) == 
            (count_total_budget(eps, refvalue, ios) + (-eps)))
        _(assert count_total_budget(eps, refvalue, iost) == (cbudget - eps))
        _(fold budget_and_trace_inv(cbudget - eps, eps, refvalue; iost))

        
        ncell->lat = nlat;
        ncell->lon = nlon;
        ncell->nxt = s;
        
        _(apply membership_endlist(e, ios);)
        _(assert member(e, iost) <=> true)
        _(assert 
          &ncell->lat |-> nlat && (nlat :: low) &&
          &ncell->lon |-> nlon && (nlon :: low) &&
          (member(e, iost) <=> true) &&
          e.est == Consumed && e.noislat == nlat && e.noislon == nlon &&
          &ncell->nxt |-> s) 
        _(fold location_server_inv(eps, refvalue; ncell, iost))
        return Success;
        
  }
  else
  {
    return Failure;

  } 

}


/* Least-Squares Linear Estimation code. Inspired by:
   https://stackoverflow.com/questions/5083465/fast-efficient-least-squares-fit-algorithm-in-c.
   
   This function and the below one, extrapolate_points, extrapolated the new location of a user, 
   based on the previously released locations, when there is not enough privacy budget. 

   */

void compute_the_values_recursively(struct ulocation *s, double eps, double refvalue, 
  double *sumx, double *sumx2, double *sumxy, double *sumy, double *sumy2, int *n)
_(requires sumx :: low && sumx2 :: low && sumxy :: low && sumy :: low && sumy2 :: low && n :: low)
_(requires exists double xoval. sumx |-> xoval && xoval :: low)
_(requires exists double x2oval. sumx2 |-> x2oval && x2oval :: low)
_(requires exists double xyoval. sumxy |-> xyoval && xyoval :: low)
_(requires exists double yoval. sumy |-> yoval && yoval :: low)
_(requires exists double y2oval. sumy2 |-> y2oval && y2oval :: low)
_(requires exists int noval. n |-> noval && noval :: low)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires location_server_inv(eps, refvalue; s, ios))
_(ensures location_server_inv(eps, refvalue; s, ios))
_(ensures io_trace(;ios))
_(ensures exists double xnval. sumx |-> xnval && xnval :: low)
_(ensures exists double x2nval. sumx2 |-> x2nval && x2nval :: low)
_(ensures exists double xynval. sumxy |-> xynval && xynval :: low)
_(ensures exists double ynval. sumy |-> ynval && ynval :: low)
_(ensures exists double y2nval. sumy2 |-> y2nval && y2nval :: low)
_(ensures exists int nnval. n |-> nnval && nnval :: low)
{
  _(unfold location_server_inv(eps, refvalue; s, ios))
  if(s == NULL)
  {
     _(fold location_server_inv(eps, refvalue; s, ios))
    return;
  }
  else
  {
    _(assert s != null)
    _(assert exists double vlat, double vlon, struct ulocation *next.    
      &s->lat |-> vlat && (vlat :: low) &&
      &s->lon |-> vlon && (vlon :: low) && 
      &s->nxt |-> next && location_server_inv(eps, refvalue; next, ios))

    *sumx += s->lat; 
    *sumx2 += s->lat * s->lat;
    *sumxy += s->lat * s->lon;
    *sumy += s->lon;
    *sumy2 += s->lon * s->lon;  
    *n = *n + 1;

    compute_the_values_recursively(s->nxt, eps, refvalue, sumx, sumx2, sumxy, sumy, sumy2, n);
    _(fold location_server_inv(eps, refvalue; s, ios))
    return;
  }
}


status_code extrapolate_points(struct ulocation *s, struct point *p, double eps, double refvalue, 
  double *sumx, double *sumx2, double *sumxy, double *sumy, double *sumy2, int *n)
_(requires s != null)
_(requires p :: low)
_(requires sumx :: low && sumx2 :: low && sumxy :: low && sumy :: low && sumy2 :: low && n :: low)
_(requires exists double oplat, double oplon. &p->lat |-> oplat
    && &p->lon |-> oplon)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires location_server_inv(eps, refvalue; s, ios))
_(requires exists double xoval. sumx |-> xoval && xoval :: low)
_(requires exists double x2oval. sumx2 |-> x2oval && x2oval :: low)
_(requires exists double xyoval. sumxy |-> xyoval && xyoval :: low)
_(requires exists double yoval. sumy |-> yoval && yoval :: low)
_(requires exists double y2oval. sumy2 |-> y2oval && y2oval :: low)
_(requires exists int noval. n |-> noval && noval :: low)
_(ensures result :: low)
_(ensures result == Success || result == Failure)
_(ensures result == Success ==> exists double nplat, double nplon. &p->lat |-> nplat 
  && &p->lon |-> nplon && nplat :: low && nplon :: low)
_(ensures result == Failure ==> &p->lat |-> oplat && &p->lon |-> oplon)
_(ensures location_server_inv(eps, refvalue; s, ios))
_(ensures io_trace(;ios))
_(ensures exists double xnval. sumx |-> xnval && xnval :: low)
_(ensures exists double x2nval. sumx2 |-> x2nval && x2nval :: low)
_(ensures exists double xynval. sumxy |-> xynval && xynval :: low)
_(ensures exists double ynval. sumy |-> ynval && ynval :: low)
_(ensures exists double y2nval. sumy2 |-> y2nval && y2nval :: low)
_(ensures exists int nnval. n |-> nnval && nnval :: low)
{
  *sumx = 0;
  *sumx2 = 0;
  *sumxy = 0;
  *sumy = 0;
  *sumy2 = 0;
  *n = 0;

  compute_the_values_recursively(s, eps, refvalue, sumx, sumx2, sumxy, sumy, sumy2, n);
  
  double denom = *n * *sumx2 - *sumx * *sumx;
  if (denom == 0)
  {
    // singular matrix and can not solve the problem
    return Failure;
  }

  double m = (*n * *sumxy - *sumx * *sumy)/denom;
  double b = (*sumy * *sumx2 - *sumx * *sumxy)/denom;
  p->lat = m;
  p->lon = b;
  return Success;
}

/* end of least-squares extrapolation code */

double sqrt(double x);
  _(ensures x :: low ==> result :: low)

/* This function uses past released (so public) locations to predict the
   user's next location. It can be used when the privacy budget is 
   exhausted if a user location is required to be reported, without incurring
   any additional privacy loss. */
status_code release_predicted_location(struct ulocation *s, double *budget, struct point *p, double eps, double refvalue, int prediction_count, 
  double *sumx, double *sumx2, double *sumxy, double *sumy, double *sumy2, int *n)
_(requires p :: low)
_(requires sumx :: low && sumx2 :: low && sumxy :: low && sumy :: low && sumy2 :: low && n :: low)
_(requires exists double xoval. sumx |-> xoval && xoval :: low)
_(requires exists double x2oval. sumx2 |-> x2oval && x2oval :: low)
_(requires exists double xyoval. sumxy |-> xyoval && xyoval :: low)
_(requires exists double yoval. sumy |-> yoval && yoval :: low)
_(requires exists double y2oval. sumy2 |-> y2oval && y2oval :: low)
_(requires exists int noval. n |-> noval && noval :: low)
_(requires prediction_count >= 0 && prediction_count :: low)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires location_server_inv(eps, refvalue; s, ios))
_(requires exists double obudget. budget |-> obudget)
_(requires budget_and_trace_inv(obudget, eps, refvalue; ios))
_(requires exists double oplat, double oplon. &p->lat |-> oplat
    && &p->lon |-> oplon)
_(ensures result :: low)   
_(ensures result == Success || result == Failure)
_(ensures budget |-> obudget)
_(ensures result == Success ==> exists double nplat, double nplon. &p->lat |-> nplat 
  && &p->lon |-> nplon && nplat :: low && nplon :: low)
_(ensures result == Failure ==> &p->lat |-> oplat && &p->lon |-> oplon)
_(ensures location_server_inv(eps, refvalue; s, ios))
_(ensures budget_and_trace_inv(obudget, eps, refvalue; ios))
_(ensures io_trace(;ios))
_(ensures exists double xnval. sumx |-> xnval && xnval :: low)
_(ensures exists double x2nval. sumx2 |-> x2nval && x2nval :: low)
_(ensures exists double xynval. sumxy |-> xynval && xynval :: low)
_(ensures exists double ynval. sumy |-> ynval && ynval :: low)
_(ensures exists double y2nval. sumy2 |-> y2nval && y2nval :: low)
_(ensures exists int nnval. n |-> nnval && nnval :: low)
{
  _(unfold location_server_inv(eps, refvalue; s, ios))
  if(s == NULL)
  {
    // there is no value in the database, 
    // so we can not return anything
    _(fold location_server_inv(eps, refvalue; s, ios))
    return Failure; 
  }
  else
  {

    _(fold location_server_inv(eps, refvalue; s, ios))
    status_code st = extrapolate_points(s, p, eps, refvalue, sumx, sumx2, sumxy, sumy, sumy2, n);
    if(st == Success)
    {
      // we have m = p->lat 
      // b = p->lon
      _(unfold location_server_inv(eps, refvalue; s, ios))
      _(assert exists double vlat, double vlon.  
        &s->lat |-> vlat && (vlat :: low) &&
        &s->lon |-> vlon && (vlon :: low))
      double m = p->lat;
      double b = p->lon;
      
      /* We assume a constant velocity of 0.01. 
         latdisp is the latitude displacement. longdisp is the longitude displacement.
         We know that:
         1. latdisp^2 + longdisp^2 = ((prediction_count+1)*0.01)^2  (assuming constant velocity)
         2. longdisp/latdisp = m  (from the least squares estimation)
         Hence (from 2): longdisp = m*latdisp
         Using this we can rewrite 1. as follows (writing D for ((prediction_count+1)*0.01)^2)
         latdisp*latdisp + (m*latdisp)*(m*latdisp) = D
         latdisp^2 + m^2*laptdisp^2 = D
         (m^2 + 1)*(latdisp^2) = D
         latdisp^2 = (D / (m^2 + 1))
         Hence: latdisp = +/- sqrt(D / (m^2 + 1))
      */
      double denom = m*m + 1;
      /* consider randomising the sign, we currently assume non-negative
         latitude displacement always */
      double latdisp = sqrt((prediction_count+1) * (prediction_count+1) / (100 * 100 * denom));

      p->lat = s->lat + latdisp;
      p->lon = m * p->lat + b;
      _(fold location_server_inv(eps, refvalue; s, ios))
    }
    else
    {
      _(assert st == Failure)
      _(unfold location_server_inv(eps, refvalue; s, ios))
      _(assert exists double vlat, double vlon.  
        &s->lat |-> vlat && (vlat :: low) &&
        &s->lon |-> vlon && (vlon :: low))
      
      // simply copy the head of list
      p->lat = s->lat; 
      p->lon = s->lon;
      _(fold location_server_inv(eps, refvalue; s, ios))
    }

    return Success;
  }
}

/* compute the length of the database of publicly released locations */
int database_length(struct ulocation *s, double eps, double refvalue)
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires location_server_inv(eps, refvalue; s, ios))
_(ensures result :: low)
_(ensures io_trace(;ios))
_(ensures location_server_inv(eps, refvalue; s, ios))
{
  _(unfold location_server_inv(eps, refvalue; s, ios))
  if(s == NULL)
  {
    _(fold location_server_inv(eps, refvalue; s, ios))
    return 0;
  }
  else
  {
    _(assert s != null)
    _(assert exists double vlat, double vlon, struct ulocation *next.    
      &s->lat |-> vlat && (vlat :: low) &&
      &s->lon |-> vlon && (vlon :: low) && 
      &s->nxt |-> next && location_server_inv(eps, refvalue; next, ios))
    int ret = database_length(s->nxt, eps, refvalue);
    _(fold location_server_inv(eps, refvalue; s, ios))
    return ret + 1;
  }
}


struct ulocation *s;
struct point *p;
double *budget;
double eps; 
double refvalue;
double *sumx;
double *sumx2; 
double *sumxy; 
double *sumy;
double *sumy2;
int *n;


void sleep(int delay);
_(requires delay :: low)

/*
Resources we get when we acquire the lock. */
void acquire_lock();
_(ensures exists list<struct event> ios. io_trace(;ios))
_(ensures location_server_inv(eps, refvalue; s, ios))
_(ensures refvalue :: low)
_(ensures 0 < eps && eps :: low)
_(ensures budget :: low && p :: low)
_(ensures sumx :: low && sumx2 :: low && sumxy :: low && sumy :: low && sumy2 :: low && n :: low)
_(ensures exists double obudget. budget |-> obudget && obudget :: low)
_(ensures budget_and_trace_inv(obudget, eps, refvalue; ios))
_(ensures exists double nplat, double nplon. &p->lat |-> nplat && &p->lon |-> nplon && nplat :: low && nplon :: low)
_(ensures exists double xnval. sumx |-> xnval && xnval :: low)
_(ensures exists double x2nval. sumx2 |-> x2nval && x2nval :: low)
_(ensures exists double xynval. sumxy |-> xynval && xynval :: low)
_(ensures exists double ynval. sumy |-> ynval && ynval :: low)
_(ensures exists double y2nval. sumy2 |-> y2nval && y2nval :: low)
_(ensures exists int nnval. n |-> nnval && nnval :: low)

/*
We need to release all the resoureces which we acquired during the lock */
void release_lock();
_(requires exists list<struct event> ios. io_trace(;ios))
_(requires location_server_inv(eps, refvalue; s, ios))
_(requires refvalue :: low)
_(requires 0 < eps && eps :: low)
_(requires budget :: low && p :: low)
_(requires sumx :: low && sumx2 :: low && sumxy :: low && sumy :: low && sumy2 :: low && n :: low)
_(requires exists double obudget. budget |-> obudget && obudget :: low)
_(requires budget_and_trace_inv(obudget, eps, refvalue; ios))
_(requires exists double oplat, double oplon. &p->lat |-> oplat && &p->lon |-> oplon && oplat :: low && oplon :: low)
_(requires exists double xoval. sumx |-> xoval && xoval :: low)
_(requires exists double x2oval. sumx2 |-> x2oval && x2oval :: low)
_(requires exists double xyoval. sumxy |-> xyoval && xyoval :: low)
_(requires exists double yoval. sumy |-> yoval && yoval :: low)
_(requires exists double y2oval. sumy2 |-> y2oval && y2oval :: low)
_(requires exists int noval. n |-> noval && noval :: low)


/*
Thread which sleeps for 15 second, and then replenish the budget. */
void replenish_budget_thread()
{
  while(1)
  {
    acquire_lock();
    refill_budget(s, budget, eps, refvalue);
    release_lock();
    sleep(15);
  }
}

/* memory allocation */
struct ulocation* alloc_ulocation();
_(ensures result :: low)
_(ensures result != null ==> exists double vlat, double vlon, struct ulocation *next. &result->lat |-> vlat
  && &result->lon |-> vlon && &result->nxt |-> next)

/* the user's location can be printed only when it is public */
void print_location(double lat, double lon, int len, double bud);
_(requires lat :: low && lon :: low && len :: low && bud :: low)

/* 
Thread which release the public location of a user periodically and adds it 
the public database, s. */
void release_location_thread()
{
  int prediction_count = 0;
  while(1)
    _(invariant prediction_count >= 0 && prediction_count :: low)
  {
    acquire_lock();
    double bud = *budget;
    if (bud >= eps)
      {  
        struct ulocation *ncell = alloc_ulocation();
        if(ncell == NULL)
        {
          release_predicted_location(s, budget, p, eps, refvalue, prediction_count, sumx, sumx2, sumxy, sumy, sumy2, n);
          prediction_count++;
        }
        else
        {
          _(assert ncell != null)
          _(assert exists double vlat, double vlon, struct ulocation *next. &ncell->lat |-> vlat
            && &ncell->lon |-> vlon && &ncell->nxt |-> next)
          status_code st = release_current_location(s, budget, eps, refvalue, p, ncell);
          _(assert bud >= eps)
          _(assert st == Success)
          s = ncell; // copy the address in ncell to s
          prediction_count = 0;
        }
      }
    else {
      release_predicted_location(s, budget, p, eps, refvalue, prediction_count, sumx, sumx2, sumxy, sumy, sumy2, n);
      prediction_count++;
    }
    release_lock();
    sleep(1);
  }
}

/* a thread that periodically prints the user's (public) location */
void print_values_thread()
{
  while(1)
  {
    acquire_lock();
    int t = database_length(s, eps, refvalue);
    print_location(p->lat, p->lon, t, *budget);
    release_lock();
    sleep(1);
  }
}

