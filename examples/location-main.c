/* #includes excluded from source line count */
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>
#include <math.h>
#include<float.h>

#define EARTH_RADIUS 6378137.0

/* begin of stuff duplicated from verified code, not double counted */
struct ulocation
{
  double lat; 
  double lon;
  struct ulocation *nxt;
};


struct point
{
  double lat;
  double lon;
};

enum ev_status
{
  Consumed,
  Replenished
};
typedef enum ev_status event_status;


// things we record
struct event
{
  event_status est;
  double noislat; // noise added to real latitude 
  double noislon; // noise added to real longitude
};


enum ret_code 
{
  Failure,
  Success
};
typedef enum ret_code status_code;

extern struct ulocation *s;
extern struct point *p;
extern int *budget;
extern double eps; 
extern double refvalue;
extern double *sumx;
extern double *sumx2; 
extern double *sumxy; 
extern double *sumy;
extern double *sumy2;
extern int *n;

extern status_code start_the_location_server(struct ulocation *s, int *budget, double eps, double refvalue);
extern void replenish_budget_thread();
extern void release_location_thread();
extern void print_values_thread();
/* end of stuff duplicated from verified code */

/* main values */
struct ulocation *ts; struct point tp; int tbudget; double teps; double trefvalue;
double tsumx; double tsumx2;  double tsumxy;  double tsumy; double tsumy2; int tn;

/* return random value in range 0 to 1 */
double random_0_1(){
  double d = (double)arc4random();
  d /= (double)UINT32_MAX;
  return d;
}

/* Code for Polar Laplace mechanism, adapated from:
  https://github.com/chatziko/location-guard/blob/master/src/js/common/laplace.js
*/
double rad_of_deg(double ang){
  return ang * M_PI / 180;
}

double deg_of_rad(double ang){
  return ang * 180 / M_PI;
}

/// LamberW function on branch -1 (http://en.wikipedia.org/wiki/Lambert_W_function)
/*
double LambertW(double x){
  //min_diff decides when the while loop should stop
  double min_diff = 1e-10;
  
  // Equality testing on double, i.e. x == -1/M_E, is never a
  // good idea, but, again, there is no simple answer. 
  // We can write fabs (x + 1.0/M_E) <= 1e-10, but 
  // I would trust the original author assuming that 
  // he knew what he was doing with equality on doubles. The 
  // reason I trust the author because he has 
  // written a while where he uses some kind 
  // precision to exit the loop (he never compares 
  // p == q). Loop terminates when fabs (p - q) <= min_diff
  
  if (x == -1.0/M_E){
    return -1;
  } 
  else if (x < 0 && x > -1/M_E){
    double q = log(-x);
    double p = 1;
    while (fabs(p-q) > min_diff){
      p = (q*q + x/exp(q))/(q+1);
      q = (p*p + x/exp(p))/(p+1);
    }
    
    //This line decides the precision of the float number that would be returned
    return (round(1000000*q)/1000000);
  } 
  else
    return 0;
} */


// http://keithbriggs.info/software/LambertW.c
// or we can use gsl (GNU SCIENTIFIC LIBRARY)
const int dbgW=0;

double LambertW(const double z) {
  int i; 
  const double eps=4.0e-16, em1=0.3678794411714423215955237701614608; 
  double p,e,t,w;
  if (dbgW) fprintf(stderr,"LambertW: z=%g\n",z);
  if (z<-em1 || isinf(z) || isnan(z)) { 
    fprintf(stderr,"LambertW: bad argument %g, exiting.\n",z); exit(1); 
  }
  if (0.0==z) return 0.0;
  if (z<-em1+1e-4) { // series near -em1 in sqrt(q)
    double q=z+em1,r=sqrt(q),q2=q*q,q3=q2*q;
    return -1.0 +2.331643981597124203363536062168*r -1.812187885639363490240191647568*q +1.936631114492359755363277457668*r*q -2.353551201881614516821543561516*q2 +3.066858901050631912893148922704*r*q2 -4.175335600258177138854984177460*q3 +5.858023729874774148815053846119*r*q3 -8.401032217523977370984161688514*q3*q;  // error approx 1e-16
  }
  /* initial approx for iteration... */
  if (z<1.0) { /* series near 0 */
    p=sqrt(2.0*(2.7182818284590452353602874713526625*z+1.0));
    w=-1.0+p*(1.0+p*(-0.333333333333333333333+p*0.152777777777777777777777)); 
  } else 
    w=log(z); /* asymptotic */
  if (z>3.0) w-=log(w); /* useful? */
  for (i=0; i<10; i++) { /* Halley iteration */
    e=exp(w); 
    t=w*e-z;
    p=w+1.0;
    t/=e*p-0.5*(p+1.0)*t/p; 
    w-=t;
    if (fabs(t)<eps*(1.0+fabs(w))) return w; /* rel-abs error */
  }
  /* should never get here */
  fprintf(stderr,"LambertW: No convergence at z=%g, exiting.\n",z); 
  exit(1);
}


// This is the inverse cumulative polar laplacian distribution function. 
double inverseCumulativeGamma(double epsilon, double z){
  double x = (z-1) / M_E;
  return - (LambertW(x) + 1) / epsilon;
}


// returns alpha such that the noisy pos is within alpha from the real pos with
// probability at least delta
// (comes directly from the inverse cumulative of the gamma distribution)
double alphaDeltaAccuracy(double epsilon, double delta){
  return inverseCumulativeGamma(epsilon, delta);
}

// returns the average distance between the real and the noisy pos
double expectedError(double epsilon){
  return 2.0 / epsilon;
}


// http://www.movable-type.co.uk/scripts/latlong.html
struct point addVectorToPos(struct point pos, double distance, double angle){
  double ang_distance = distance / EARTH_RADIUS;
  double lat1 = rad_of_deg(pos.lat);
  double lon1 = rad_of_deg(pos.lon);

  double lat2 =	asin(sin(lat1) * cos(ang_distance) + 
                     cos(lat1) * sin(ang_distance) * cos(angle));
  double lon2 = lon1 + atan2(sin(angle) * sin(ang_distance) * cos(lat1), 
                             cos(ang_distance) - sin(lat1) * sin(lat2));

  lon2 = fmod(lon2 + 3.0 * M_PI, 2 * M_PI) - M_PI;  // normalise to -180..+180
  return (struct point) { .lat = deg_of_rad(lat2), .lon = deg_of_rad(lon2) };
}


struct point addPolarNoise(double epsilon, struct point pos){
  //random number in [0, 2*PI)
  double theta = random_0_1() * M_PI * 2;
  //random variable in [0,1)
  double z = random_0_1();
  double r = inverseCumulativeGamma(epsilon, z);

  return addVectorToPos(pos, r, theta);
}


//This function generates the position of a point with Laplacian noise
struct point addNoise(double epsilon, struct point pos){
  return addPolarNoise(epsilon, pos);
}

/* end of Polar Laplace mechanism code */

/* this function adds noise to a real location of user and 
  returns an io event */
struct event add_noise_to_real_point(struct point *p, double eps){  
  // create a temporary point and copy the current point to that
  struct point tp = *p;
  // return the stored value in p  
  *p = addNoise(eps, tp);
  return (struct event){.est = Consumed, .noislat = p->lat, .noislon = p->lon};
}

#define VELOCITY 0.01

time_t g_start;
/* We simulate a user walking in a straight line at constant VELOCITY */
void get_real_location(struct point *p){
  time_t now = time(NULL);
  double distance = (now - g_start) * VELOCITY;
  p->lat = 0.0;
  p->lon = 0.0;
  if (distance != 0.0){
  /* 2 (disp^2) = distance^2, hence disp = sqrt(distance^2/2) */
    double disp = sqrt(distance*distance/2);
    p->lat = disp;
    p->lon = disp;
  }
  printf("Secret location: %lf°N, %lf°W\n",p->lat,p->lon);
}


struct event generate_replenished_event(){
  return  (struct event) {.est = Replenished};
}

/* memory allocation */
struct ulocation* alloc_ulocation(){
  return (struct ulocation *) malloc (sizeof (struct ulocation));
}

pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

void acquire_lock(){
  pthread_mutex_lock(&g_mutex);
}

void release_lock(){
  pthread_mutex_unlock(&g_mutex);
}

void print_location(double lat, double lon, int len, double bud){
  printf("Public location: %lf°N, %lf°W, database length: %d, budget: %.1lf\n", lat, lon, len, bud);
}


int main(){
  // in the beginning s would NULL
  ts = NULL; s = ts;
  p = &tp;   budget = &tbudget; tbudget = 0; teps = 0.2; eps = teps;
  trefvalue = 1.0; refvalue = trefvalue;   n = &tn;
  sumx = &tsumx; sumx2 = &tsumx2; sumxy = &tsumxy; sumy = &tsumy; sumy2 = &tsumy2;
  g_start = time(NULL);
  start_the_location_server(s, budget, eps, refvalue);

  pthread_t replenish_thread, release_thread, print_thread;
  // start a thread which would run an infinite loop would replenish 
  // the budget after certain amount of time. 
  pthread_create(&replenish_thread, NULL, &replenish_budget_thread, NULL);
  pthread_create(&release_thread, NULL, &release_location_thread, NULL);
  pthread_create(&print_thread, NULL, &print_values_thread, NULL); 

  // the last two are useless because all these threads are running 
  // an infinite loop, so one is enough to hold this main function. 
  pthread_join(replenish_thread, NULL);
  pthread_join(release_thread, NULL);
  pthread_join(print_thread, NULL);
} 

/*
int main()
{
  printf("%.10lf\n", LambertW(-0.234));
}*/

