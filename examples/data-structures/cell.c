// Simple cell case study, possibly to be used as starting point for automatic fold/unfold

struct cell {
    int x;
};

_(predicate ce(struct cell *p; int i)
    &p->x |-> i)

int get(struct cell *p)
    _(requires ce(p; ?i))
    _(ensures  ce(p;  i) && result == i)
{
    _(unfold ce(p; i))
    int r = p->x;
    _(fold ce(p; i))
    return r;
}
void set(struct cell *p, int i)
    _(requires ce(p; ?j))
    _(ensures  ce(p; i))
{
    _(unfold ce(p; j))
    p->x = i;
    _(fold ce(p; i))
}