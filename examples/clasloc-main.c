/* #includes not included in unverified sloc count */
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

#include <sys/time.h>


/* from https://www.gnu.org/software/libc/manual/html_node/Calculating-Elapsed-Time.html
   Subtract the ‘struct timeval’ values X and Y,
   storing the result in RESULT.
   Return 1 if the difference is negative, otherwise 0. */

int
timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (x->tv_usec - y->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

/* stuff duplicated from verified code */

typedef struct {
  int clas; // 0 is low, 1 is high
  int val;
  int full; // boolean
} clasloc_t;

extern clasloc_t *g_p;
extern int *resc;
extern int *resv;

extern void thread1();
extern void thread2();

/* end of stuff duplicated from verified code */

int tresc;
int tresv;
clasloc_t loc;

struct timeval start;
void start_timer(){
  gettimeofday(&start,NULL);
}

struct timeval curr;
struct timeval diff;

void log_count(int count){
  double mib = ((double)count)*((double)4.0)/((double)1024.0*1024.0);
  gettimeofday(&curr,NULL);
  timeval_subtract(&diff, &curr, &start);      
  double elapsed = diff.tv_sec + (diff.tv_usec * 1e-6);      
  printf("%lf MiB received OK in %lf secs. %lf MiB/sec\n",mib,elapsed,mib/elapsed);  
}


pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

void acquire_lock(){
  pthread_mutex_lock(&g_mutex);
}

void release_lock(){
  pthread_mutex_unlock(&g_mutex);
}



void* handle_thread1(void *arg){
  thread1();
  return NULL;
}

void* handle_thread2(void *arg){
  thread2();
  return NULL;
}


int main(int argc, char *argv[]){
  pthread_t thread1_thread, thread2_thread;  
  // initialization of global variables of clasloc.c
  resc = &tresc;
  resv = &tresv;
  g_p = &loc;
  
  
  pthread_create(&thread1_thread, NULL, &handle_thread1, NULL);
  pthread_create(&thread2_thread, NULL, &handle_thread2, NULL);  
  pthread_join(thread1_thread,NULL);
  pthread_join(thread2_thread,NULL);
  
  return 0;
}
