#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage $0 guess"
    exit 1;
fi

echo -n "$1" |  nc -v 127.0.0.1 3000

